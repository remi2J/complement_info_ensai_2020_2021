from business_objects.monstre.monstre import Monstre
from dao.monster_dao import MonsterDao
from dao.type_monstre_dao import TypeMonstreDao


class MonstreService:

    @staticmethod
    def get_monster_from_db_by_name(monster_name):
        """
        Récupère un monstre grâce à son nom
        :param monster_name:
        :type monster_name:
        :return:
        :rtype:
        """
        return MonsterDao.find_by_name(monster_name)


    @staticmethod
    def get_all_monstres_from_db(nb_monstres=10):
        """
        Récupère un certain nombre de monstre en base
        :param nb_monstres: le nombre de monstre à récupérer
        :type nb_monstres: int
        :return: une liste de monstre
        :rtype: list of Monstre
        """
        return MonsterDao.find_all(nb_monstres)


    @staticmethod
    def add_monster_to_db(monster):
        """

        :param monster:
        :type monster:
        :return:
        :rtype:
        """

        return MonsterDao.create(monster)

    @staticmethod
    def build_monster(**dict_attributs):
        """
        Créé une instance de monstre à partir d'un dictionnaire avec les
        bonnes clefs
        :param dict_attributs:
        :type dict_attributs:
        :return:
        :rtype:
        """
        monstre = Monstre()
        if "nom" in dict_attributs:
            monstre.nom = dict_attributs["nom"]
        if "description" in dict_attributs:
            monstre.description = dict_attributs["description"]
        if "force" in dict_attributs:
            monstre.force = dict_attributs["force"]
        if "agilite" in dict_attributs:
            monstre.agilite = dict_attributs["agilite"]
        if "magie" in dict_attributs:
            monstre.magie = dict_attributs["magie"]
        if "defense" in dict_attributs:
            monstre.defense = dict_attributs["defense"]
        if "points_de_vie" in dict_attributs:
            monstre.points_de_vie = dict_attributs["points_de_vie"]
        if "type_monstre" in dict_attributs:
            type_monstre = TypeMonstreDao.find_by_name(
                dict_attributs["type_monstre"])
            if type_monstre :
                monstre.type_monstre = type_monstre
            else:
                monstre.type_monstre = TypeMonstreDao.create_name_only(
                    dict_attributs["type_monstre"])

        return monstre


    @staticmethod
    def update_monster_in_db(monster):
        """

        :param monster:
        :type monster:
        :return:
        :rtype:
        """
        return MonsterDao.update(monster)

    @staticmethod
    def delete_monstre_from_db_with_name(monster_name):
        return MonsterDao.delete(Monstre(nom=monster_name))
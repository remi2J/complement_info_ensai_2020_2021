from business_objects.objet.arme.abstract_arme import AbstractArme
from dao.arme_dao import ArmeDao


class ArmeService:
    @staticmethod
    def get_arme_from_db_by_name(nom_arme):
        """
        Récupère un monstre grâce à son nom
        :param nom_arme: le nom de l'arme cherché
        :type nom_arme: str
        :return: l'arme avec le bon nom
        :rtype: AbstractArme
        """
        return ArmeDao.find_by_name(nom_arme)

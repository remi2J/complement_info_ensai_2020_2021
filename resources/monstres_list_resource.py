from flask_restful import Resource, reqparse, marshal, fields

from business_objects.monstre.monstre import Monstre
from dao.monster_dao import MonsterDao


class MonsterListResource(Resource):
    """
    Classe Resource qui va s'occuper de la recherche général des monstres,
    soit car on veut tous les mondres, soit car on fait un recherche.
    """
    def get(self):
        """
        Récupère la liste simplifiée des monstres en bases et la retourne au
        client OU retourne le monstre avec le critère de recherche passé.
        C'est l'un des rares cas où avoir plusieurs return siplifie
        beaucoup le code car chaque return est vraiment différent.
        On pourrais faire avec un seul mais le code deviendrait
        compliqué.
        :return: la liste des monstres
        :rtype: dict
        """

        data_to_transfer = None
        try:
            # Récupération des arguments de la requête
            parser = reqparse.RequestParser()
            parser.add_argument('rows', type=int)
            parser.add_argument('q')

            if parser.parse_args()["q"]:
                # On est dans le cas d'une recherche spécifique
                query_param = parser.parse_args()["q"]

                if query_param.isdigit():
                    # On a un int alors on fait la recherche suivant un id
                    data_to_transfer = MonsterDao.find_by_id(
                        int(query_param)
                    )
                else:
                    # On a seulement un string alors on fait une recherche
                    # par nom
                    data_to_transfer = MonsterDao.find_by_name(
                        query_param
                    )

                if data_to_transfer:
                    # Si on a trouvé un montre on le retourne en pensant à le
                    # tranformer en json.
                    return MonsterListResource.monstre_to_full_json(
                        data_to_transfer), 200
                else:
                    return None, 204
            else:
                # On veut tous les résultats
                nb_rows = 10
                if parser.parse_args()["rows"]:
                    # Remplacement du paramètre rows par defaut
                    nb_rows = int(parser.parse_args()["rows"])

                # Recherche de tous les montres en base
                monsters_bdd = MonsterDao.find_all(nb_rows)

                # Création des données à transférer. Il faut transformer les
                # monstres en json
                data_to_transfer = {
                    'count': len(monsters_bdd),
                    'result': [MonsterListResource.monstre_to_lite_json(monstre)
                               for monstre in monsters_bdd]
                }
                return data_to_transfer, 200

        except Exception as e:
            return {"message": "Le chat du développeur a dû s'attaquer aux "
                               "cables du serveur"}, 400

    @staticmethod
    def monstre_to_lite_json(monstre):
        """
        Prend un monstre est en fait un dicionnaire allegé pour envoyer au
        client. Sans cela il on essaye de passer notre objet directement une
        erreur va être levée car notre objet n'est pas serialisable en json.
        Il sera possible d'ajouter une méthode _json_() à notre objet monstre
        pour le rentre serialisable, mais cela pose un problème de conception,
        pourquoi mettre dans la classe monstre la matnière de le transformer
        en json ? Cela n'a pas ça place dans notre classe. Ici c'est plus
        logique.

        Le fonctionnement de cette méthode est simple, on prendre un monstre
        et on en fait un dictionnaire.
        :param monstre: le monstre à transformer n dictionnaire
        :type monstre: Monstre
        :return: un dictionnaire
        :rtype:  dict
        """

        monstre_dict = {
            'id': monstre.id,
            'nom': monstre.nom,
            'description': monstre.description,
            'type_monstre': {
                "id_label": monstre.type_monstre.id_label,
                "label": monstre.type_monstre.label
            },
            'url': monstre.url
        }
        return monstre_dict

    @staticmethod
    def monstre_to_full_json(monstre):
        """
        Prend un monstre est en fait un dicionnaire complet pour envoyer au
        client. Sans cela il on essaye de passer notre objet directement une
        erreur va être levée car notre objet n'est pas serialisable en json.
        Il sera possible d'ajouter une méthode _json_() à notre objet monstre
        pour le rentre serialisable, mais cela pose un problème de conception,
        pourquoi mettre dans la classe monstre la matnière de le transformer
        en json ? Cela n'a pas ça place dans notre classe. Ici c'est plus
        logique.

        Le fonctionnement de cette méthode est simple, on prendre un monstre
        et on en fait un dictionnaire.
        :param monstre: le monstre à transformer n dictionnaire
        :type monstre: Monstre
        :return: un dictionnaire
        :rtype:  dict
        """
        monstre_dict = {
            'id': monstre.id,
            'nom': monstre.nom,
            'description': monstre.description,
            'type_monstre': {
                "id_label": monstre.type_monstre.id_label,
                "label": monstre.type_monstre.label
            },
            'statistique': {
                'points_de_vie': monstre.points_de_vie,
                'force': monstre.force,
                'magie': monstre.magie,
                'agilite': monstre.agilite,
                'defense': monstre.defense
            },
            'url': monstre.url
        }

        return monstre_dict

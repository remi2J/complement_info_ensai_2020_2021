from flask import request
from flask_restful import Resource

from service.monstre_service import MonstreService


class MonsterResource(Resource):
    """
    Classe Resource qui va s'occuper de manipuler une ressource monstre
    spécifique.
    - lecture : méthode get
    - creation : méthode put
    - mise à jour : méthode post
    - supression : méthode delete
    """

    def get(self, monster_name):
        """
        Récupère toutes les informations sur du monstre dont le nom est passé
        en paramètre. Si aucun montre n'a ce nom, un code 4040 est retourne
        :param monster_name : le nom de la ressource monstre que l'on cherche
        :type monster_name : str
        :return: la réponse à la requête httpo
        :rtype: dict
        """
        try:
            # Le monstre est cherché en base de donnée
            data_to_transfer = MonstreService.get_monster_from_db_by_name(
                monster_name)

            # Si un monstre est trouvé il est envoyé au clien avec avoir
            # était serialisé.
            if data_to_transfer:
                return MonsterResource.monstre_to_full_json(
                    data_to_transfer), 201

            # Sinon une 404 est envoyée
            else:
                return {"message": "Aucun monstre ne se cache ici"}, 404
        except Exception as e:
            print(e)
            return {"message": "Oups il s'est passé quelque chose "
                               "d'imprévisible"}, 400

    def put(self, monster_name):
        """
        Ajoute une ressource monstre aux ressources accessibles
        :param monster_name : le nom de la ressource monstre que l'on veut
        ajouter
        :type monster_name : str
        :return: la réponse à la requête http
        :rtype: dict
        """
        monstre = None
        try:
            # Verification que toutes les clefs attendues sont bien passées
            # dans la requête/
            clefs = ["description", "force", "agilite", "magie", "defense",
                     "points_de_vie", "type_monstre"]
            if all(clef in request.json for clef in clefs):
                # Si toutes les clefs sont dans la requête on appelle le service

                dict_attribut = request.json
                dict_attribut["nom"] = monster_name
                monstre = MonstreService.build_monster(
                    **dict_attribut)
                MonstreService.add_monster_to_db(monstre)

            else:
                # S'il manque des clefs le web service envoie un message
                # d'erreur et un code 400/
                return {"message": "Il manque des paramètres dans la requête"}, \
                       400
        except KeyError:
            return {"message": "Il y a une erreur dans la requête"}, 400

        except Exception as error:
            print(error)
            return {"message": "Erreur interne"}, 500

        return MonsterResource.monstre_to_full_json(
            monstre), 201

    def post(self, monster_name):
        """
        Met à jour la ressource monstre en fonction du corps de la requête.
        :param monster_name : le nom de la ressource monstre que l'on veut
        modifier
        :type monster_name : str
        :return: la réponse à la requête httpo
        :rtype: dict
        """
        try:
            # Recupération du corps de la requête
            dict_attribut = request.json
            # On modifie le nom pour qu'il soit cohérent avec celui de le
            # ressource
            dict_attribut["nom"] = monster_name
            # Création d'un objet monstre
            monstre = MonstreService.build_monster(
                **dict_attribut)
            # Mise à jour du montsre en base de donné
            MonstreService.update_monster_in_db(monstre)

            # Si tout ce passe bien on retroune le monstre et un code 200
            return MonsterResource.monstre_to_full_json(
                monstre), 200

        except Exception as error:
            print(error)
            return {"message": "Erreur interne"}, 500

    def delete(self, monster_name):
        """
        Supprime une ressource monstre du système de persistance
        :param monster_name: le nom du montre à supprimer
        :type monster_name: str
        :return: une requête http
        :rtype:
        """
        try:
            if MonstreService.delete_monstre_from_db_with_name(monster_name):
                data_to_transfer = {"message": "Le montre a été supprimsé"}
            else:
                data_to_transfer = {"message": "Aucun monstre à supprimer"}
            return data_to_transfer, 200
        except Exception:
            return {"message": "Erreur interne"}, 500

    @staticmethod
    def monstre_to_full_json(monstre):
        """
        Prend un monstre est en fait un dicionnaire complet pour envoyer au
        client. Sans cela il on essaye de passer notre objet directement une
        erreur va être levée car notre objet n'est pas serialisable en json.
        Il sera possible d'ajouter une méthode _json_() à notre objet monstre
        pour le rentre serialisable, mais cela pose un problème de conception,
        pourquoi mettre dans la classe monstre la matnière de le transformer
        en json ? Cela n'a pas ça place dans notre classe. Ici c'est plus
        logique.

        Le fonctionnement de cette méthode est simple, on prendre un monstre
        et on en fait un dictionnaire.
        :param monstre: le monstre à transformer n dictionnaire
        :type monstre: Monstre
        :return: un dictionnaire
        :rtype:  dict
        """
        monstre_dict = {
            'id': monstre.id,
            'nom': monstre.nom,
            'description': monstre.description,
            'type_monstre': {
                "id_label": monstre.type_monstre.id_label,
                "label": monstre.type_monstre.label
            },
            'statistique': {
                'points_de_vie': monstre.points_de_vie,
                'force': monstre.force,
                'magie': monstre.magie,
                'agilite': monstre.agilite,
                'defense': monstre.defense
            },
            'url': monstre.url
        }

        return monstre_dict

from flask_restful import Resource

from business_objects.objet.arme.abstract_arme import AbstractArme
from service.arme_service import ArmeService



class ArmeResource(Resource):
    def get(self, nom_arme):
        """
        Récupère toutes les informations sur une amre
        :param nom_arme : le nom de la ressource montre que l'on cherche
        :type nom_arme : str
        :return: la réponse à la requête http
        :rtype: dict
        """
        try:
            data_to_transfer = ArmeService.get_arme_from_db_by_name(nom_arme)

            if data_to_transfer:
                return ArmeResource.arme_to_full_json(
                    data_to_transfer), 201
            else:
                return {"message": "Aucun monstre ne se cache ici"}, 404
        except Exception as e:
            print(e)
            return {"message": "Oups il s'est passé quelque chose "
                               "d'imprévisible"}, 400

    @staticmethod
    def arme_to_full_json(arme):
        """
        Prend une  est en fait un dicionnaire  pour envoyer au client
        :param arme: le monstre à transformer en dictionnaire
        :type arme: AbstractArme
        :return: un dictionnaire
        :rtype:  dict
        """
        arme_dict = {
            'id': arme.id,
            'nom': arme.nom,
            'description_arraque': arme.description_attaque,
            'base_attaque' : arme.base_attaque,
            'type': arme.DATABASE_TYPE_LABEL,
            'statistique': {
                'points_de_vie': arme.points_de_vie,
                'force': arme.force,
                'magie': arme.magie,
                'agilite': arme.agilite,
                'defense': arme.defense
            },
            'url': arme.url
        }
        return arme_dict
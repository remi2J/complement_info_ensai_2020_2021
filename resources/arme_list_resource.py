from flask_restful import Resource, reqparse

from business_objects.objet.arme.abstract_arme import AbstractArme
from dao.arme_dao import ArmeDao


class ArmeListResource(Resource):
    def get(self):
        """
         Récupère la liste simplifiée des armes en bases et la retourne au
         client.
         :return: la liste des arme
         :rtype: dict of AbstractArme
         """

        data_to_transfer = None
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('rows', type=int)

            nb_rows = 10
            if parser.parse_args()["rows"]:
                # On a un paramètre nb_rows
                nb_rows = int(parser.parse_args()["rows"])

            arme_bdd = ArmeDao.find_all(nb_rows)

            data_to_transfer = {
                'count': len(arme_bdd),
                'result': [ArmeListResource.arme_to_lite_json(arme) for
                           arme in arme_bdd]
            }
            return data_to_transfer, 200

        except Exception as e:
            return {"message": "Le chat du développeur a dû s'attaquer aux "
                               "cables du serveur"}, 400

    @staticmethod
    def arme_to_lite_json(arme):
        """
        Prend une  est en fait un dicionnaire alleger  pour envoyer au client
        :param arme: le monstre à transformer en dictionnaire
        :type arme: AbstractArme
        :return: un dictionnaire
        :rtype:  dict
        """
        arme_dict = {
            'id': arme.id,
            'nom': arme.nom,
            'description_arraque': arme.description_attaque,
            'base_attaque': arme.base_attaque,
            'type': arme.DATABASE_TYPE_LABEL,
            'url': arme.url
        }
        return arme_dict

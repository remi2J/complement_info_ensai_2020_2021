from dotenv import load_dotenv

from configuration import properties
from webservice_python_rpg import app

if __name__ == "__main__":
    # Import variable env
    load_dotenv()

    app.run(debug=True)

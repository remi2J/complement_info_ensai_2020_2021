import psycopg2

from dao.abstract_dao import AbstractDao
from dao.pool_connection import PoolConnection

from business_objects.personnage.abstract_personnage import AbstractPersonnage
from factory.personnage_factory import PersonnageFactory


class PersonnageDao(AbstractDao):

    @staticmethod
    def create(personnage):
        """
        Ajoute un personnagedans notre base de données s'il n'a pas d'personnage.
        :param personnage: le personnage à ajouter
        :type personnage: AbstractPersonnage
        :return: le personnage mis à jour de son id
        :rtype: AbstractPersonnage
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # On envoie au serveur la requête SQL
            curseur.execute(
                "INSERT INTO personnage (type_personnage,nom,force, agilite, "
                "magie, "
                "defense, points_de_vie)"
                " VALUES (%s, %s, %s, %s, %s, %s) RETURNING id_personnage;"
                , (personnage.NOM_BASE_DE_DONNEE
                   , personnage.nom
                   , personnage.statistique.force
                   , personnage.statistique.agilite
                   , personnage.statistique.magie
                   , personnage.statistique.defense
                   , personnage.statistique.points_de_vie))
            # On récupère l'id généré
            personnage.id = curseur.fetchone()["id_personnage"]



            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return personnage

    @staticmethod
    def delete(personnage):
        """
        Supprime un personnage de la base
        :param personnage : le personnage à supprimer
        :type personnage: AbstractPersonnage
        :return: si la suppresion a été faite
        :rtype: bool
        """
        deleted = False
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # On envoie au serveur la requête SQL
            curseur.execute(
                "DELETE FROM personnage WHERE id_personnage=%s;"
                , (personnage.id,))

            # on verifie s'il y a eu des supressions
            if curseur.rowcount > 0:
                deleted = True

            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return deleted

    @staticmethod
    def find_by_id(id):
        """
        Va chercher un personnage dont on connait l'id.
        :param id : l'id de l'élément que l'on cherche
        :type id : int
        :return le personnage avec le bon id
        :rtype AbstractPersonnage
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT id_personnage, type_personnage, nom"
                "force, agilite,  magie, defense, points_de_vie "
                "\n\t FROM personnage "
                "\n\t WHERE id_personnage= %s"
                , (id,))
            resultat = curseur.fetchone()
            personnage = None
            # Si on a un résultat
            if resultat:
                personnage = PersonnageFactory.generer_personnage(
                    type=resultat["type_personnage"]
                    , id=resultat["id_personnage"]
                    , nom=resultat["nom"]
                    , force=resultat["force"]
                    , agilite=resultat["agilite"]
                    , magie=resultat["magie"]
                    , defense=resultat["defense"]
                    , points_de_vie=resultat["points_de_vie"])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return personnage

    @staticmethod
    def find_all():
        """
        Va chercher toutes les personnage en base.
        :return Toutes les personnages de la base
        :rtype list of AbstractPersonnage
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT id_personnage, type_personnage, nom,"
                "force, agilite,  magie, defense, points_de_vie "
                "\n\t FROM personnage"
            )
            resultats = curseur.fetchall()
            personnages = []
            for resultat in resultats:
                personnages.append(
                    PersonnageFactory.generer_personnage(
                        type=resultat["type_personnage"]
                        , id=resultat["id_personnage"]
                        , nom=resultat["nom"]
                        , force=resultat["force"]
                        , agilite=resultat["agilite"]
                        , magie=resultat["magie"]
                        , defense=resultat["defense"]
                        , points_de_vie=resultat["points_de_vie"])
                )
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return personnages

    @staticmethod
    def update(personnage):
        """
        Met à jour un pesonnage dans la base de données. On met à jour la ligne
        avec l'id du personnage en paramètre
        :param personnage: l'arme à mettre à jour
        :type personnage: AbstractPersonnage
        :return: si la mise à jour a été faite
        :rtype: bool
        """
        updated = False
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "UPDATE personnage"
                "\n\t SET type_personnage = %s"
                "\n\t,arme_id = %s"
                "\n\t,nom  = %s"
                "\n\t,force  = %s"
                "\n\t,agilite  = %s "
                "\n\t,magie  = %s"
                "\n\t,defense   = %s"
                "\n\t,points_de_vie   = %s"
                "\n\t WHERE id_personnage=%s ;"
                , (personnage.NOM_BASE_DE_DONNEE
                   , personnage.arme.id
                   , personnage.nom
                   , personnage.statistique.force
                   , personnage.statistique.agilite
                   , personnage.statistique.magie
                   , personnage.statistique.defense
                   , personnage.statistique.points_de_vie
                   , personnage.id))

            if curseur.rowcount > 0:
                updated = True


            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return updated
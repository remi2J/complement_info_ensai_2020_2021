from dao.abstract_dao import AbstractDao
from dao.pool_connection import PoolConnection


class TypeArmeDao(AbstractDao):

    @staticmethod
    def find_id_by_label(label):
        """
        Retourne l'id d'un __label d'personnage
        :param label: le _nom du type d'personnage
        :type label: str
        :return: l'id associé au __label
        :rtype: int
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT type_arme_id "
                "FROM type_arme "
                "WHERE type_arme_label = LOWER(%s)"
                , (label,))
            resultat = curseur.fetchone()
            id_type_arme = None
            if resultat :
                id_type_arme = resultat["type_arme_id"]
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return id_type_arme


    @staticmethod
    def find_all():
        """
        Requête la base de données pour obtenir tous les couples id-__label de
        type d'personnage
        :return: Un dictionnaire avec le contenu de la table type_arme
        :rtype: dict
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT type_arme_label,type_arme_id "
                "FROM type_arme")
            resultats = curseur.fetchall()
            type_arme = {}
            for type in resultats:
                type_arme[type["type_arme_label"]] = type["type_arme_id"]
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return type_arme
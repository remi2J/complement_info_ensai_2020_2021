import psycopg2

from business_objects.monstre.monstre import Monstre
from dao.abstract_dao import AbstractDao
from dao.pool_connection import PoolConnection
from dao.statistique_monster_dao import StatistiqueMonsterDao
from dao.type_monstre_dao import TypeMonstreDao


class MonsterDao(AbstractDao):
    @staticmethod
    def find_all(nb_row=100):
        """
        Va chercher au maximum 1000 les monstres en base.
        :return : Toutes les monstres de la base
        :rtype  : list of Monstre
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        nb_max = min(nb_row, 1000)
        try:
            curseur.execute(
                "SELECT m.id_monstre, nom, description, "
                "label_type_monstre, m.type_monstre, force, magie, agilite, "
                "defense, points_de_vie"
                "\n\t FROM monstre as m "
                "\n\t\t LEFT JOIN type_monstre ON  m.type_monstre = "
                "type_monstre.id_type_monstre "
                "LIMIT %(limit)s"
                , {"limit": nb_max}
            )
            resultats = curseur.fetchall()
            monstres = []
            for resultat in resultats:
                monstres.append(
                    Monstre(
                        id=resultat["id_monstre"]
                        , nom=resultat["nom"]
                        , force=resultat["force"]
                        , agilite=resultat["agilite"]
                        , magie=resultat["magie"]
                        , defense=resultat["defense"]
                        , points_de_vie=resultat["points_de_vie"]
                        , description=resultat["description"]
                        , id_type_monstre=resultat["type_monstre"]
                        , label_type_monstre=resultat["label_type_monstre"]
                    )
                )
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return monstres

    @staticmethod
    def find_by_name(name):
        """
        Va chercher un monstre en base avec som nom.
        :param name : le nom du monstre que l'on cherche
        :type name : str
        :return : le monstre avec le nom passé en paramètre
        :rtype : Monstre
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT m.id_monstre,nom, description,m.type_monstre, "
                "label_type_monstre, force, agilite, magie, defense, points_de_vie "
                "\n\t FROM (SELECT * from monstre where nom LIKE %(nom)s)  as m "
                "\n\t\t LEFT JOIN type_monstre ON  m.type_monstre = "
                "type_monstre.id_type_monstre ",
                {"nom": name.lower()}
            )
            resultat = curseur.fetchone()
            monstre = None
            # Si on a un résultat
            if resultat:
                monstre = Monstre(
                    id=resultat["id_monstre"]
                    , nom=resultat["nom"]
                    , description=resultat["description"]
                    , id_type_monstre=resultat["type_monstre"]
                    , label_type_monstre=resultat["label_type_monstre"]
                    , force=resultat["force"]
                    , agilite=resultat["agilite"]
                    , magie=resultat["magie"]
                    , defense=resultat["defense"]
                    , points_de_vie=resultat["points_de_vie"]
                )
        except psycopg2.Error as error:
            print(error)
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return monstre

    @staticmethod
    def find_by_id(id):
        """
        Va chercher un monstre en base avec son id.
        :param id : le nom du monstre que l'on cherche
        :type id : str
        :return : le monstre avec le nom passé en paramètre
        :rtype  : Monstre
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT m.id_monstre,nom, description,m.type_monstre, "
                "label_type_monstre, force, agilite, magie, defense, points_de_vie "
                "\n\t FROM (SELECT * FROM monstre where id_monstre = %(id)s)  "
                "as m "
                "\n\t\t LEFT JOIN type_monstre ON  m.type_monstre = "
                "type_monstre.id_type_monstre ",
            {"id": id})
            resultat = curseur.fetchone()
            monstre = None
            # Si on a un résultat
            if resultat:
                monstre = Monstre(
                    id=resultat["id_monstre"]
                    , nom=resultat["nom"]
                    , description=resultat["description"]
                    , id_type_monstre=resultat["type_monstre"]
                    , label_type_monstre=resultat["label_type_monstre"]
                    , force=resultat["force"]
                    , agilite=resultat["agilite"]
                    , magie=resultat["magie"]
                    , defense=resultat["defense"]
                    , points_de_vie=resultat["points_de_vie"]
                )
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return monstre

    @staticmethod
    def update(monstre_to_update):
        """
        Met à jour un monstre en base à partir du montre passé en paramètre
        :param monstre_to_update: le monstre à mettre à jour
        :type monstre_to_update: Monstre
        :return:
        :rtype:
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "UPDATE monstre"
                "\n\t SET "
                "\n\t\t description = %(description)s,"
                "\n\t\t type_monstre = %(type)s,"
                "\n\t\t nom = %(nom)s,"
                "\n\t\t force = %(force)s,"
                "\n\t\t agilite = %(agilite)s,"
                "\n\t\t magie = %(magie)s,"
                "\n\t\t defense = %(defense)s,"
                "\n\t\t points_de_vie = %(pdv)s"
                "\n\t WHERE nom LIKE %(nom)s"
                "\n\t RETURNING id_monstre"
                , {"nom": monstre_to_update.nom
                    , "description": monstre_to_update.description
                    , "type": monstre_to_update.type_monstre.id_label
                    , "force": monstre_to_update.force
                    , "agilite": monstre_to_update.agilite
                    , "magie": monstre_to_update.magie
                    , "defense": monstre_to_update.defense
                    , "pdv": monstre_to_update.points_de_vie})
            resultat = curseur.fetchone()
            monstre_to_update.id=resultat["id_monstre"]
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return monstre_to_update

    @staticmethod
    def update_monster_core_info(curseur, monstre_to_update):
        """
        S'occupe de la génération de la requête pour mettre à jour les
        informations de base d'un monstre. La partie compliquée est la
        génération automatique de la requête.
        :param curseur: le cuseur utilisé pour les requête
        :type curseur: cursor
        :param monstre_to_update: le montre à mettre à jour
        :type monstre_to_update: Monstre
        :return: rien
        :rtype: None
        """
        requete_SQL = "UPDATE monstre \n SET "
        parametres = []
        if monstre_to_update.description is not None:
            # maj description
            requete_SQL = requete_SQL + "\n\t description = %s,"
            parametres.append(monstre_to_update.description)
        if monstre_to_update.type_monstre.__id_type is not None:
            # maj __id_type
            requete_SQL = requete_SQL + "\n\t type_monstre = %s,"
            parametres.append(monstre_to_update.type_monstre.id_label)
        if monstre_to_update.nom is not None:
            # maj nom
            requete_SQL = requete_SQL + "\n\t nom = %s,"
            parametres.append(monstre_to_update.nom)
        # on retire la virugle finale
        requete_SQL = requete_SQL[:-1]
        # finalisation de la requête
        requete_SQL = requete_SQL + " \nWHERE nom LIKE %s"
        requete_SQL = requete_SQL + " \nRETURNING id_monstre, nom, " \
                                    "description, type_monstre"
        parametres.append(monstre_to_update.nom)

        # exécution de la requête
        curseur.execute(requete_SQL, tuple(parametres))
        # on récupère le résultat
        resultat = curseur.fetchone()
        monstre_to_update.nom = resultat["nom"]
        monstre_to_update.description = resultat["description"]
        monstre_to_update.id = resultat["id_monstre"]
        monstre_to_update.type_monstre = TypeMonstreDao.find_by_id(
            resultat["id_monstre"])

    @staticmethod
    def create(monster):
        """
        Ajoute une monstre en base
        :param monster : le  monstre que l'on souhaite ajouter
        :type monster : Monstre
        :return le monstre mis à jour des infos gérées par la base
        :rtype : Monstre
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "INSERT INTO monstre (nom, description,type_monstre,force,"
                " agilite, magie, defense, points_de_vie)"
                " VALUES (%(nom)s, %(description)s, %(type)s, %(force)s, "
                "%(agilite)s, %(magie)s, %(defense)s, %(pdv)s)"
                " RETURNING id_monstre;"
                , {"nom": monster.nom
                    , "description": monster.description
                    , "type": monster.type_monstre.id_label
                    , "force": monster.force
                    , "agilite": monster.agilite
                    , "magie": monster.magie
                    , "defense": monster.defense
                    , "pdv": monster.points_de_vie})
            # On récupère l'id généré
            monster.id = curseur.fetchone()["id_monstre"]

            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return monster

    @staticmethod
    def delete(monster):
        """
        Supprime une monstre en base. Le critère de suppression est seulement le nom du monstre
        :param monster : le  monstre que l'on souhaite supprimer
        :type monster : Monstre
        :return si la supression a été faite
        :rtype : bool
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        deleted = False
        try:
            curseur.execute(
                "DELETE FROM monstre where nom = %s;"
                , (monster.nom,))

            # on verifie s'il y a eu des supressions
            if curseur.rowcount > 0:
                deleted = True

            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return deleted

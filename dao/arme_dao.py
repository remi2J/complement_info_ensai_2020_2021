import psycopg2

from dao.abstract_dao import AbstractDao
from dao.pool_connection import PoolConnection
from business_objects.objet.arme.abstract_arme import AbstractArme
from dao.type_arme_dao import TypeArmeDao
from factory.arme_factory import ArmeFactory


class ArmeDao(AbstractDao):

    @staticmethod
    def create(arme):
        """
        Ajouter une arme dans notre base de données.
        :param arme: l'arme à ajouter
        :type arme: AbstractArme
        :return: l'personnage mise à jour de son id
        :rtype: AbstractArme
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # On récupère l'id du type d'personnage
            id_label = TypeArmeDao.find_id_by_label(arme.DATABASE_TYPE_LABEL)

            # On envoie au serveur la requête SQL
            curseur.execute(
                "INSERT INTO arme (nom, description_attaque,"
                "base_attaque, id_label,force, agilite, magie, "
                "defense, points_de_vie)"
                " VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING "
                "id_arme;"
                , (arme.nom
                   , arme.description_attaque
                   , arme.base_attaque
                   , id_label
                   , arme.statistique_bonus.force
                   , arme.statistique_bonus.agilite
                   , arme.statistique_bonus.magie
                   , arme.statistique_bonus.defense
                   , arme.statistique_bonus.points_de_vie))

            # On récupère l'id généré
            arme.id = curseur.fetchone()["id_arme"]

            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return arme

    @staticmethod
    def delete(arme):
        """
        Supprime une arme de la base
        :param arme: l'arme à supprimer
        :type arme: AbstractArme
        :return: si la suppresion a été faite
        :rtype: bool
        """
        deleted = False
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # On envoie au serveur la requête SQL
            curseur.execute(
                "DELETE FROM arme WHERE id_arme=%s;"
                , (arme.id,))
            # attention quand vous n'avez qu'un champ il faut garder une
            # structure de tuple et donc bien mettre un virgule avec
            # rien derrière

            # on verifie s'il y a eu des supressions
            if curseur.rowcount > 0:
                deleted = True

            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return deleted

    @staticmethod
    def find_by_id(id):
        """
        Va chercher une arme dont on connait l'id.
        :param id : l'id de l'élément que l'on cherche
        :type id : int
        :return l'arme avec le bon id
        :rtype AbstractArme
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # Petite optimisation dans la requête SQL. On réalise une requête
            # intermédiaire pour filtrer les données que l'on veut récupérer
            # et ensuite on fait la jointure. Si dans la cas présent ce n'est
            # pas une grande optimisation, quand on manipule des Go de données
            # cela peut faire gagner du temps. De manière générale il faut
            # filtrer les données le plus tôt possible puis les traiter ensuite
            curseur.execute(
                "SELECT armes_filteree.id_arme, nom, description_attaque,"
                "base_attaque,type_arme_label, force, agilite, magie, "
                "defense, points_de_vie "
                "\n\t FROM (SELECT *"
                "\n\t FROM arme "
                "\n\t WHERE id_arme = %s) as armes_filteree"
                "\n\t INNER JOIN type_arme ON armes_filteree.id_label = "
                "type_arme.type_arme_id "
                , (id,))
            resultat = curseur.fetchone()
            arme = None
            # Si on a un résultat
            if resultat:
                arme = ArmeFactory.generer_arme(
                    type=resultat["type_arme_label"]
                    , id=resultat["id_arme"]
                    , nom=resultat["nom"]
                    , description_attaque=resultat["description_attaque"]
                    , base_attaque=resultat["base_attaque"]
                    , force_bonus=resultat["force"]
                    , agilite_bonus=resultat["agilite"]
                    , magie_bonus=resultat["magie"]
                    , defense_bonus=resultat["defense"]
                    , points_de_vie_bonus=resultat["points_de_vie"])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return arme

    @staticmethod
    def find_by_name(nom):
        """
        Va chercher une arme dont on connait le nom.
        :param nom : le nomde l'élément que l'on cherche
        :type nom : str
        :return l'arme avec le bon nom
        :rtype AbstractArme
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # Petite optimisation dans la requête SQL. On réalise une requête
            # intermédiaire pour filtrer les données que l'on veut récupérer
            # et ensuite on fait la jointure. Si dans la cas présent ce n'est
            # pas une grande optimisation, quand on manipule des Go de données
            # cela peut faire gagner du temps. De manière générale il faut
            # filtrer les données le plus tôt possible puis les traiter ensuite
            curseur.execute(
                "SELECT armes_filteree.id_arme, nom, description_attaque,"
                "base_attaque,type_arme_label, force, agilite, magie, "
                "defense, points_de_vie "
                "\n\t FROM (SELECT *"
                "\n\t FROM arme "
                "\n\t WHERE nom LIKE %(nom)s) as armes_filteree"
                "\n\t INNER JOIN type_arme ON armes_filteree.id_label = "
                "type_arme.type_arme_id "
                , {"nom" : nom})
            resultat = curseur.fetchone()
            arme = None
            # Si on a un résultat
            if resultat:
                arme = ArmeFactory.generer_arme(
                    type=resultat["type_arme_label"]
                    , id=resultat["id_arme"]
                    , nom=resultat["nom"]
                    , description_attaque=resultat["description_attaque"]
                    , base_attaque=resultat["base_attaque"]
                    , force_bonus=resultat["force"]
                    , agilite_bonus=resultat["agilite"]
                    , magie_bonus=resultat["magie"]
                    , defense_bonus=resultat["defense"]
                    , points_de_vie_bonus=resultat["points_de_vie"])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return arme

    @staticmethod
    def find_all(nombre_enregistrement = 0):
        """
        Va chercher toutes les armes en base.
        :return Toutes les armes de la base
        :rtype list of AbstractArme
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            requete_sql = "SELECT arme.id_arme, nom, description_attaque," \
                          "base_attaque,type_arme_label, force, agilite, magie," \
                          "defense, points_de_vie "\
                          "\n\t FROM arme"\
                          "\n\t INNER JOIN " \
                          "type_arme ON arme.id_label = type_arme.type_arme_id "
            if nombre_enregistrement :
                requete_sql = requete_sql +\
                              "\n\t LIMIT " + str(nombre_enregistrement)
            curseur.execute(requete_sql)
            resultats = curseur.fetchall()
            armes = []
            for resultat in resultats:
                armes.append(
                    ArmeFactory.generer_arme(
                        type=resultat["type_arme_label"]
                        , id=resultat["id_arme"]
                        , nom=resultat["nom"]
                        , description_attaque=resultat["description_attaque"]
                        , base_attaque=resultat["base_attaque"]
                        , force_bonus=resultat["force"]
                        , agilite_bonus=resultat["agilite"]
                        , magie_bonus=resultat["magie"]
                        , defense_bonus=resultat["defense"]
                        , points_de_vie_bonus=resultat["points_de_vie"])
                )
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return armes

    @staticmethod
    def update(arme):
        """
        Met à jour une personnage dans la base de données. On met à jour la ligne
        avec l'id de l'personnage en paramètre
        :param arme: l'personnage à mettre à jour
        :type arme: AbstractArme
        :return: si la mise à jour a été faite
        :rtype: bool
        """
        updated = False
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "UPDATE arme"
                "\n\t SET nom = %s"
                "\n\t,description_attaque = %s"
                "\n\t,base_attaque = %s"
                "\n\t,force  = %s "
                "\n\t,agilite  = %s "
                "\n\t,magie  = %s"
                "\n\t,defense   = %s"
                "\n\t,points_de_vie   = %s"
                "\n\t WHERE id_arme=%s ;"
                , (arme.nom
                   , arme.description_attaque
                   , arme.base_attaque
                   , arme.statistique_bonus.force
                   , arme.statistique_bonus.agilite
                   , arme.statistique_bonus.magie
                   , arme.statistique_bonus.defense
                   , arme.statistique_bonus.points_de_vie
                   , arme.id))

            if curseur.rowcount > 0:
                updated = True

            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return updated

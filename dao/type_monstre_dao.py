import psycopg2

from business_objects.monstre.type_monstre import TypeMonstre
from dao.abstract_dao import AbstractDao
from dao.pool_connection import PoolConnection


class TypeMonstreDao(AbstractDao):
    @staticmethod
    def find_all():
        """
        Va chercher tous les types de monstre en base.
        :return : Toutes les types de monstres en base
        :rtype  : list of TypeMonstre
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT id_type_monstre, label_type_monstre "
                "\n\t FROM type_monstre "
            )
            resultats = curseur.fetchall()
            types_monstre = []
            for resultat in resultats:
                types_monstre.append(
                    TypeMonstre(
                        id_type=resultat["id_type_monstre"]
                        , label=resultat["label_type_monstre"]
                    )
                )
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return types_monstre

    @staticmethod
    def find_by_name(type_name):
        """
        Va chercher l'id d'un type à partir de son nom. On fait une recherche
        approximative pour accépter les fautes d'orthographe
        :param type_name : le __label du type que l'on cherche
        :type type_name : str
        :return : le type complet
        :rtype  : TypeMonstre
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT id_type_monstre, label_type_monstre "
                "\n\t FROM type_monstre "
                "\n\t WHERE label_type_monstre LIKE %s ;"
                # recherche approximative
                , (type_name,)
            )
            resultat = curseur.fetchone()
            type_monstre = None
            if resultat:
                type_monstre = TypeMonstre(
                    id_type=resultat["id_type_monstre"]
                    , label=resultat["label_type_monstre"]
                )
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return type_monstre

    @staticmethod
    def find_by_id(type_id):
        """
        Va chercher un type de monstre à partir d'un id
        :param type_id : l'id du type que l'on cherche
        :type type_id : int
        :return : le type complet
        :rtype  : TypeMonstre
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT id_type_monstre, label_type_monstre "
                "\n\t FROM type_monstre "
                "\n\t WHERE id_type_monstre = %s ;"
                # recherche approximative
                , (type_id,)
            )
            resultat = curseur.fetchone()
            type_monstre = None
            if resultat:
                type_monstre = TypeMonstre(
                    id_type=resultat["id_type_monstre"]
                    , label=resultat["label_type_monstre"]
                )
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return type_monstre

    @staticmethod
    def update(business_object):
        pass

    @staticmethod
    def create(type_monstre_to_create):
        """
        Ajoute un type de montre en base. Ne présupose pas de l'id ajouté, va
        récupérer celui donné par la base
        :param type_monstre_to_create : la type de montre à ajouter
        :type type_monstre_to_create : TypeMonstre
        :return : Le type de montre mis à jour de son id
        :rtype  : TypeMonstre
        """

        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "INSERT INTO type_monstre (label_type_monstre) "
                "\n\t VALUES (%s) "
                "\n\t RETURNING id_type_monstre"
                , (type_monstre_to_create.label,)
            )
            resultat = curseur.fetchone()
            if resultat:
                type_monstre_to_create.id_label = resultat['id_type_monstre']
                # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return type_monstre_to_create

    @staticmethod
    def create_name_only(type_monstre_to_create_name):
        """
        Ajoute un type de monstre en base à partir d'un nom.
        :param type_monstre_to_create_name : le nom du type de montre à ajouter
        :type type_monstre_to_create_name : str
        :return : Le type de monstre tel qu'il est en base
        :rtype  : TypeMonstre
        """
        return TypeMonstreDao.create(TypeMonstre(type_monstre_to_create_name))

    @staticmethod
    def delete(business_object):
        pass

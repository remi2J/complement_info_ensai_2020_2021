import psycopg2

from dao.abstract_dao import AbstractDao
from dao.pool_connection import PoolConnection
from business_objects.monstre.monstre import Monstre

class StatistiqueMonsterDao(AbstractDao):
    @staticmethod
    def find_all():
        pass

    @staticmethod
    def find_by_name(name):
        pass

    @staticmethod
    def update(monster_to_update, connexion=None):
        """
        Met à jour les statistique d'un monstre en base
        :param monster_to_update: le monstre dont il faut mettre à jour les
        statistique
        :type monster_to_update: Monstre
        :param connexion: une connexion à la base de données. Potentiellement
        on va devoir réutiliser une connection déjà existante. Dans ce cas là on
        ne la ferme pas à la fin
        :type connexion:
        :return: le monstre avec tout ces champs
        :rtype: Monstre
        """
        close_connection_at_the_end = False
        if connexion is None :
            connexion = PoolConnection.getConnexion()
            close_connection_at_the_end = True

        curseur = connexion.cursor()
        requete_SQL = "UPDATE statistique_monstre \n SET "
        parametres = []

        if monster_to_update.force is not None:
            requete_SQL = requete_SQL + "\n\t force = %s,"
            parametres.append(monster_to_update.force)
        if monster_to_update.agilite is not None:
            requete_SQL = requete_SQL + "\n\t agilite = %s,"
            parametres.append(monster_to_update.agilite)
        if monster_to_update.magie is not None:
            requete_SQL = requete_SQL + "\n\t magie = %s,"
            parametres.append(monster_to_update.magie)
        if monster_to_update.points_de_vie is not None:
            requete_SQL = requete_SQL + "\n\t points_de_vie = %s,"
            parametres.append(monster_to_update.points_de_vie)
        if monster_to_update.defense is not None:
            requete_SQL = requete_SQL + "\n\t defense = %s,"
            parametres.append(monster_to_update.defense)

        # on retire la virugle finale
        requete_SQL = requete_SQL[:-1]

        # finalisation de la requête
        requete_SQL = requete_SQL + " \nWHERE id_monstre = %s"
        requete_SQL = requete_SQL + " \nRETURNING force, agilite, " \
                                    "magie, defense, points_de_vie ;"
        parametres.append(monster_to_update.id)

        try :
            # exécution de la requête
            curseur.execute(requete_SQL, tuple(parametres))
            # on récupère le résultat
            resultat = curseur.fetchone()
            monster_to_update.force = resultat["force"]
            monster_to_update.agilite = resultat["agilite"]
            monster_to_update.magie = resultat["magie"]
            monster_to_update.defense = resultat["defense"]
            monster_to_update.points_de_vie = resultat["points_de_vie"]

            # On commit la requête que si on doit fermer la requête à la fin.
            # Sinon c'est de la responsabilité de l'appelant
            if close_connection_at_the_end:
                connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            if close_connection_at_the_end:
                curseur.close()
                PoolConnection.putBackConnexion(connexion)
        return monster_to_update

    @staticmethod
    def create(business_object):
        pass


    @staticmethod
    def delete(business_object):
        pass
from business_objects.personnage.abstract_personnage import AbstractPersonnage
from business_objects.attaque_info import AttaqueInfo
from exceptions.arme_interdite_exception import ArmeInterditeException


class PersonnageService:
    """
    PesonnageService va manipuler les personnages pour qu'ils agissent. Toutes
    les méthodes de la classe sont statiques car PersonnageService n'a pas
    d'état. Il va seulement manipuler les personnages qu'on lui passe en
    paramètre de ces fonctions.
    """

    @staticmethod
    def attaquer(personnage):
        """
        Détermine à partir d'un personnage son attaque (texte et dégât)
        :param personnage: le personnage qui attaque
        :type personnage: AbstractPersonnage
        :return: l'attaque effectuée
        :rtype: AttaqueInfo
        """
        if not personnage.arme.__class__ in personnage.ARMES_AUTORISEES:
            raise ArmeInterditeException(personnage, personnage.arme)

        return AttaqueInfo(phrase_attaque=personnage.arme.description_attaque,
                           degats_attaque=personnage.degats_attaque())

    @staticmethod
    def defendre(personnage, attaque_info):
        """
        Détermine à partir d'un personnage le texte et sa défense
        :param personnage: le personnage qui defend
        :type personnage: AbstractPersonnage
        :param attaque_info : les informations de l'attaque
        :type attaque_info : AttaqueInfo
        :return: l'attaque effectuée mise à jour de la défense
        :rtype: AttaqueInfo
        """
        attaque_info.phrase_defense = personnage.armure.description_defense
        attaque_info.degats_finaux = personnage.reduction_degats(
            attaque_info.degats_attaque
        )
        return attaque_info


    @staticmethod
    def changer_arme(personnage, arme):
        """
        Changer l'personnage d'une personnage
        :param personnage: Le personne qui va recevoir une nouvelle personnage
        :type personnage: AbstractPersonnage
        :param arme: La nouvelle personnage
        :type arme: Arme
        :return: None
        """
        personnage.arme=arme

from business_objects.personnage.abstract_personnage import AbstractPersonnage
from business_objects.personnage.guerrier import Guerrier
from business_objects.personnage.magicien import Magicien
from business_objects.personnage.voleur import Voleur


class PersonnageFactory:
    """
    Classe qui implèmente le patern factory. Elle nous permet d'instacier des
    armes sans que l'on ait à savoir comment elle fait.
    """

    @staticmethod
    def generer_personnage(type,
                           id=None
                           , nom=""
                           , force=0
                           , agilite=0
                           , magie=0
                           , defense=0
                           , points_de_vie=0
                           ):
        """
        Va créer un personnage à partir des paramètres passés
        :param type: le type de l'personnage. Va déterminer sa classe
        :type type: str
        :param id: l'id du personnage (optionnel)
        :type id: int
        :param force:(optionnel)
        :type force: int
        :param agilit: (optionnel)
        :type agilite: int
        :param magie: (optionnel)
        :type magie: int
        :param defense: (optionnel)
        :type defense: int
        :param points_de_vie: (optionnel)
        :type points_de_vie: int
        :return: le personnage créee
        :rtype: AbstractPersonnage
        """
        personnage = None
        if type == Guerrier.NOM_BASE_DE_DONNEE:
            personnage = Guerrier(id=id
                                  , nom=nom
                                  , force=force
                                  , agilite=agilite
                                  , magie=magie
                                  , defense=defense
                                  , points_de_vie=points_de_vie)
        elif type == Voleur.NOM_BASE_DE_DONNEE:
            personnage = Voleur(id=id
                                , nom=nom
                                , force=force
                                , agilite=agilite
                                , magie=magie
                                , defense=defense
                                , points_de_vie=points_de_vie)
        elif type == Magicien.NOM_BASE_DE_DONNEE:
            personnage = Magicien(id=id
                                  , nom=nom
                                  , force=force
                                  , agilite=agilite
                                  , magie=magie
                                  , defense=defense
                                  , points_de_vie=points_de_vie)

        return personnage

from business_objects.objet.arme.abstract_arme import AbstractArme
from business_objects.objet.arme.arc import Arc
from business_objects.objet.arme.baton import Baton
from business_objects.objet.arme.couteau import Couteau
from business_objects.objet.arme.epee import Epee
from business_objects.objet.arme.lance import Lance
from business_objects.objet.arme.livre import Livre


class ArmeFactory:
    """
    Classe qui implèmente le patern factory. Elle nous permet d'instacier des
    armes sans que l'on ait à savoir comment elle fait.
    """

    @staticmethod
    def generer_arme(type,
                     id=None
                     , nom=""
                     , description_attaque=""
                     , base_attaque=0
                     , force_bonus=0
                     , agilite_bonus=0
                     , magie_bonus=0
                     , defense_bonus=0
                     , points_de_vie_bonus=0
                     ):
        """
        Va créer une personnage à partir des paramètres passés
        :param type: le type de l'personnage. Va déterminer sa classe
        :type type: str
        :param id: l'id de l'personnage (optionnel)
        :type id: int
        :param nom: le _nom de l'personnage (optionnel)
        :type nom: str
        :param description_attaque: la __description de l'attaque (optionnel)
        :type description_attaque: str
        :param base_attaque: les dégâts de base de l'personnage
        :type base_attaque: int
        :param force_bonus:(optionnel)
        :type force_bonus: int
        :param agilite_bonus: (optionnel)
        :type agilite_bonus: int
        :param magie_bonus: (optionnel)
        :type magie_bonus: int
        :param defense_bonus: (optionnel)
        :type defense_bonus: int
        :param points_de_vie_bonus: (optionnel)
        :type points_de_vie_bonus: int
        :return: l'personnage créee
        :rtype: AbstractArme
        """
        arme = None
        if type == Arc.DATABASE_TYPE_LABEL:
            arme = Arc(id=id
                       , nom=nom
                       , description_attaque=description_attaque
                       , base_attaque=base_attaque
                       , force_bonus=force_bonus
                       , agilite_bonus=agilite_bonus
                       , magie_bonus=magie_bonus
                       , defense_bonus=defense_bonus
                       , points_de_vie_bonus=points_de_vie_bonus)
        elif type == Baton.DATABASE_TYPE_LABEL:
            arme = Baton(id=id
                         , nom=nom
                         , description_attaque=description_attaque
                         , base_attaque=base_attaque
                         , force_bonus=force_bonus
                         , agilite_bonus=agilite_bonus
                         , magie_bonus=magie_bonus
                         , defense_bonus=defense_bonus
                         , points_de_vie_bonus=points_de_vie_bonus)
        elif type == Couteau.DATABASE_TYPE_LABEL:
            arme = Couteau(id=id
                           , nom=nom
                           , description_attaque=description_attaque
                           , base_attaque=base_attaque
                           , force_bonus=force_bonus
                           , agilite_bonus=agilite_bonus
                           , magie_bonus=magie_bonus
                           , defense_bonus=defense_bonus
                           , points_de_vie_bonus=points_de_vie_bonus)
        elif type == Epee.DATABASE_TYPE_LABEL:
            arme = Epee(id=id
                        , nom=nom
                        , description_attaque=description_attaque
                        , base_attaque=base_attaque
                        , force_bonus=force_bonus
                        , agilite_bonus=agilite_bonus
                        , magie_bonus=magie_bonus
                        , defense_bonus=defense_bonus
                        , points_de_vie_bonus=points_de_vie_bonus)
        elif type == Lance.DATABASE_TYPE_LABEL:
            arme = Lance(id=id
                         , nom=nom
                         , description_attaque=description_attaque
                         , base_attaque=base_attaque
                         , force_bonus=force_bonus
                         , agilite_bonus=agilite_bonus
                         , magie_bonus=magie_bonus
                         , defense_bonus=defense_bonus
                         , points_de_vie_bonus=points_de_vie_bonus)
        elif type == Livre.DATABASE_TYPE_LABEL:
            arme = Livre(id=id
                         , nom=nom
                         , description_attaque=description_attaque
                         , base_attaque=base_attaque
                         , force_bonus=force_bonus
                         , agilite_bonus=agilite_bonus
                         , magie_bonus=magie_bonus
                         , defense_bonus=defense_bonus
                         , points_de_vie_bonus=points_de_vie_bonus)
        return arme

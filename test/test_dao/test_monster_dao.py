from unittest import TestCase

from business_objects.monstre.monstre import Monstre
from dao.monster_dao import MonsterDao


class TestMonsterDao(TestCase):
    def test_create(self):
        # GIVEN
        monstre_a_cree = Monstre(
            nom="monstre test"
            , description="un monstre de test"
            , id_type_monstre=1
            , points_de_vie=10
            , force=10
            , magie=10
            , agilite=10
            , defense=10
        )

        # WHEN
        monstre_cree = MonsterDao.create(monstre_a_cree)
        MonsterDao.delete(monstre_cree)

        # THEN
        self.assertIsNotNone(monstre_cree)

    def test_update(self):
        # GIVEN
        monstre_a_cree = Monstre(
            nom="monstre test"
            , description="un monstre de test"
            , id_type_monstre=1
            , points_de_vie=10
            , force=10
            , magie=10
            , agilite=10
            , defense=10
        )
        MonsterDao.create(monstre_a_cree)

        monstre_mettre_à_jour = Monstre(
            id=1
            , nom="monstre test"
            , description="best test ever"
            , id_type_monstre=1
        )
        # WHEN
        monstre_mise_a_jour = MonsterDao.update(monstre_mettre_à_jour)
        MonsterDao.delete(monstre_mettre_à_jour)

        # THEN
        self.assertEqual("best test ever", monstre_mise_a_jour.description)

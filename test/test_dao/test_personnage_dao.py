from unittest import TestCase

from business_objects.personnage.guerrier import Guerrier
from business_objects.personnage.magicien import Magicien
from dao.personnage_dao import PersonnageDao


class TestPersonnageDao(TestCase):
    def test_create(self):
        """
        On teste l'insertion d'un personnage en verifiant que l'on a
        bien un id pour noter personange.
        Puis on supprime la ligne. Cette méthode à beaucoup de problèmes,
        mais avec les outils à votre disposition il est difficile de faire
        mieux.
        """
        # GIVEN
        link = Guerrier(force=10, agilite=20, magie=30, defense=40,
                        points_de_vie=50)

        # WHEN
        link_new = PersonnageDao.create(link)

        # THEN
        self.assertIsNotNone(link_new.id)
        PersonnageDao.delete(link_new)

    def test_delete(self):
        """
        On teste la suppression en verifiant que l'on supprime bien la ligne
        que l'on vient d'insérer. Cette méthode à beaucoup de problèmes,
        mais avec les outils à votre disposition il est difficile de faire
        mieux.
        """
        # GIVEN
        link = Guerrier(force=10, agilite=20, magie=30, defense=40,
                        points_de_vie=50)
        # WHEN
        link_new = PersonnageDao.create(link)
        deleted = PersonnageDao.delete(link_new)

        # THEN
        self.assertTrue(deleted)

    def test_find_by_id_guerrier(self):
        # GIVEN
        link = Guerrier(force=10, agilite=20, magie=30, defense=40,
                        points_de_vie=50)

        # WHEN
        link_new = PersonnageDao.create(link)

        linkd_db = PersonnageDao.find_by_id(link_new.id)
        # On supprime la ligne
        PersonnageDao.delete(link_new)

        # THEN
        self.assertIsInstance(linkd_db, Guerrier,
                              "n'est pas de la bonne classe")
        self.assertEqual(link_new.id, linkd_db.id,
                         "problème dans les id")


    def test_find_by_id_magicien(self):
        # GIVEN
        aang = Magicien(force=10, agilite=20, magie=30, defense=40,
                        points_de_vie=50)

        # WHEN
        aang_new = PersonnageDao.create(aang)

        aang_db = PersonnageDao.find_by_id(aang_new.id)
        # On supprime la ligne
        PersonnageDao.delete(aang_new)

        # THEN
        self.assertIsInstance(aang_db, Magicien,
                              "n'est pas de la bonne classe")
        self.assertEqual(aang_new.id, aang_db.id,
                         "problème dans les id")

    def test_find_all_personnages(self):
        # GIVEN

        # WHEN
        personnage = PersonnageDao.find_all()
        # THEN
        self.assertIsNotNone(personnage, "aucun personnage trouvé")
from unittest import TestCase

from dao.pool_connection import PoolConnection


class TestPoolConnection(TestCase):
    def test_getInstance(self):
        # GIVEN
        reservoir_connexion = PoolConnection.getInstance()

        # THEN
        self.assertIsNotNone(reservoir_connexion)




    def test_getConnexion(self):
        # GIVEN
        connexion = PoolConnection.getConnexion()

        # THEN
        self.assertIsNotNone(connexion)

    def test_getConnexion_basic_select(self):
        """
        Un test pour vérifier si la requete "select 1" fonctionne
        """
        # GIVEN
        connexion = PoolConnection.getConnexion()

        #WHEN
        curseur = connexion.cursor()
        curseur.execute("select 1 as col")
        resultat = curseur.fetchall()

        # THEN
        self.assertEqual(1,resultat[0]["col"])
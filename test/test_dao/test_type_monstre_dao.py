from unittest import TestCase

from business_objects.monstre.type_monstre import TypeMonstre
from dao.type_monstre_dao import TypeMonstreDao


class TestTypeMonstreDao(TestCase):
    def test_find_all(self):
        # GIVEN

        # WHEN
        types = TypeMonstreDao.find_all()
        # THEN
        self.assertIsNotNone(types)

    def test_find_by_name(self):
        # GIVEN

        # WHEN
        types = TypeMonstreDao.find_by_name(type_name="animal")
        # THEN
        self.assertIsNotNone(types)

    def test_create(self):
        # GIVEN
        type_montre = TypeMonstre(label="test")
        # WHEN
        TypeMonstreDao.create(type_montre)
        # THEN
        self.assertIsNotNone(type_montre.__id_type)

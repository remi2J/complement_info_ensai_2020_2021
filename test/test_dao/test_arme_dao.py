from unittest import TestCase

from business_objects.objet.arme.epee import Epee
from business_objects.objet.arme.lance import Lance
from dao.arme_dao import ArmeDao


class TestArmeDao(TestCase):
    def test_create(self):
        """
        On teste l'insertion en verifiant que l'on a bien un id pour notre personnage.
        Puis on supprime la ligne. Cette méthode à beaucoup de problèmes,
        mais avec les outils à votre disposition il est difficile de faire
        mieux.
        """
        # GIVEN
        master_sword = Epee(
            nom="Master sword"
            , force_bonus=0
            , agilite_bonus=0
            , magie_bonus=0
            , defense_bonus=0
            , points_de_vie_bonus=0
            , description_attaque="En éclair lumineux s'abatit sur le monstre"
            , base_attaque=750)

        # WHEN
        master_sword_new = ArmeDao.create(master_sword)

        # THEN
        self.assertIsNotNone(master_sword_new.id)
        ArmeDao.delete(master_sword_new)


    def test_delete(self):
        """
        On teste la suppression en verifiant que l'on supprime bien la ligne
        que l'on vient d'insérer. Cette méthode à beaucoup de problèmes,
        mais avec les outils à votre disposition il est difficile de faire
        mieux.
        """
        # GIVEN
        master_sword = Epee(
            nom="Master sword"
            , force_bonus=0
            , agilite_bonus=0
            , magie_bonus=0
            , defense_bonus=0
            , points_de_vie_bonus=0
            ,
            description_attaque="En éclair lumineux s'abatit sur le monstre"
            , base_attaque=750)

        # WHEN
        master_sword_new = ArmeDao.create(master_sword)
        deleted = ArmeDao.delete(master_sword_new)

        # THEN
        self.assertTrue(deleted)


    def test_update_arme(self):
        master_sword = Epee(
            nom="Master sword"
            , force_bonus=0
            , agilite_bonus=0
            , magie_bonus=0
            , defense_bonus=0
            , points_de_vie_bonus=0
            , description_attaque="En éclair lumineux s'abatit sur le monstre"
            , base_attaque=750)

        # WHEN
        master_sword_new = ArmeDao.create(master_sword)
        master_sword_new.nom = "Epée du prodige"
        master_sword_new.description_attaque = "test"
        master_sword_new.base_attaque = 800
        updated = ArmeDao.update(master_sword_new)
        ArmeDao.delete(master_sword_new)

        # THEN
        self.assertTrue(updated)

    def test_find_by_id_arme_epee(self):
        # GIVEN
        master_sword = Epee(
            nom="Master sword"
            , force_bonus=0
            , agilite_bonus=0
            , magie_bonus=0
            , defense_bonus=0
            , points_de_vie_bonus=0
            , description_attaque="En éclair lumineux s'abatit sur le monstre"
            , base_attaque=750)

        # WHEN
        master_sword_new = ArmeDao.create(master_sword)

        master_sword_db = ArmeDao.find_by_id(master_sword_new.id)
        # On supprime la ligne
        ArmeDao.delete(master_sword)

        # THEN
        self.assertIsInstance(master_sword_db, Epee,
                         "n'est pas de la bonne classe")
        self.assertEqual(master_sword_new.id, master_sword_db.id,
                         "problemèe dans les id")
        self.assertEqual(master_sword_new.nom, master_sword_db.nom,
                         "problème dans les _nom")
        self.assertEqual(master_sword_new.description_attaque, master_sword_db.description_attaque,
                         "problemèe dans les description_attaque")
        self.assertEqual(master_sword_new.base_attaque, master_sword_db.base_attaque,
                         "problemèe dans les base_attaque")

    def test_find_by_id_arme_lance(self):
        # GIVEN
        trident = Lance(
            nom="Trident Zora"
            , force_bonus=0
            , agilite_bonus=0
            , magie_bonus=0
            , defense_bonus=0
            , points_de_vie_bonus=0
            ,
            description_attaque="En éclair lumineux s'abatit sur le monstre"
            , base_attaque=750)

        # WHEN
        trident_new = ArmeDao.create(trident)

        trident_db = ArmeDao.find_by_id(trident_new.id)
        # On supprime la ligne
        ArmeDao.delete(trident)

        # THEN
        self.assertIsInstance(trident_db, Lance,
                              "n'est pas de la bonne classe")
        self.assertEqual(trident_new.id, trident_db.id,
                         "problemèe dans les id")
        self.assertEqual(trident_new.nom, trident_db.nom,
                         "problemèe dans les _nom")
        self.assertEqual(trident_new.description_attaque,
                         trident_db.description_attaque,
                         "problemèe dans les description_attaque")
        self.assertEqual(trident_new.base_attaque, trident_db.base_attaque,
                         "problemèe dans les base_attaque")

    def test_find_all_arme(self):
        # GIVEN

        # WHEN
        armes = ArmeDao.find_all()
        # THEN
        self.assertIsNotNone(armes, "aucune personnage trouvé")

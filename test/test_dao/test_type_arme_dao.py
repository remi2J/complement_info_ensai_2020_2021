from unittest import TestCase

from dao.type_arme_dao import TypeArmeDao


class TestTypeArmeDao(TestCase):
    def test_find_id_by_label_ok(self):
        # GIVEN
        label = "epee"
        # WHEN
        id = TypeArmeDao.find_id_by_label(label)
        # THEN
        self.assertIsNotNone(id, "l'id est None")

    def test_find_id_by_label_ko(self):
        # GIVEN
        label = "qsdfqsfdqsdf"
        # WHEN
        id = TypeArmeDao.find_id_by_label(label)
        # THEN
        self.assertIsNone(id, "Un id a été trouvé alors qu'il n'y en a pas")


    def test_find_all(self):
        # GIVEN

        # WHEN
        types_arme = TypeArmeDao.find_all()
        # THEN
        self.assertIsNotNone(types_arme, "Aucun type trouvé")

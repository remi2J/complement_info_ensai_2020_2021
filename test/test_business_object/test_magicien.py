from unittest import TestCase

from business_objects.personnage.magicien import Magicien


class TestMagicien(TestCase):
    def test_creation_magicien_force(self):
        # GIVEN
        expected_output = 10
        magicien = Magicien(
            force=expected_output
            , agilite=0
            , magie=0
            , defense=0
            , points_de_vie=0)
        # WHEN
        actual_output = magicien.statistique.force
        # THEN
        self.assertEqual(expected_output, actual_output)

    def test_creation_magicien_agilite(self):
        # GIVEN
        expected_output = 10
        magicien = Magicien(
            force=0
            , agilite=expected_output
            , magie=0
            , defense=0
            , points_de_vie=0)
        # WHEN
        actual_output = magicien.statistique.agilite
        # THEN
        self.assertEqual(expected_output, actual_output)

    def test_creation_magicien_magie(self):
        # GIVEN
        expected_output = 10
        magicien = Magicien(
            force=0
            , agilite=0
            , magie=expected_output
            , defense=0
            , points_de_vie=0)
        # WHEN
        actual_output = magicien.statistique.magie
        # THEN
        self.assertEqual(expected_output, actual_output)

    def test_creation_magicien_defense(self):
        # GIVEN
        expected_output = 10
        magicien = Magicien(
            force=0
            , agilite=0
            , magie=0
            , defense=expected_output
            , points_de_vie=0)
        # WHEN
        actual_output = magicien.statistique.defense
        # THEN
        self.assertEqual(expected_output, actual_output)

    def test_creation_magicien_pdv(self):
        # GIVEN
        expected_output = 10
        magicien = Magicien(
            force=0
            , agilite=0
            , magie=0
            , defense=0
            , points_de_vie=expected_output)
        # WHEN
        actual_output = magicien.statistique.points_de_vie
        # THEN
        self.assertEqual(expected_output, actual_output)
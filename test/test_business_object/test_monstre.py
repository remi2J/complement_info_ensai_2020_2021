from unittest import TestCase

from business_objects.element.monstre import Monstre


class TestMonstre(TestCase):
    def test_set_force(self):
        """Test un peu stupide pour vérifier que le getter force fonctionne"""
        # GIVEN
        monstre = Monstre()
        # WHEN
        expected_force = 10
        monstre.force = expected_force
        # THEN
        self.assertEqual(expected_force, monstre._statistique.force)


    def test_get_force(self):
        """Test un peu stupide pour vérifier que le setter force fonctionne"""
        # GIVEN
        expected_force = 10
        monstre = Monstre(force=expected_force)
        # WHEN
        # THEN
        self.assertEqual(monstre.force, monstre._statistique.force)

    def test_set_agilite(self):
        # GIVEN
        monstre = Monstre()
        # WHEN
        expected_agilite = 10
        monstre.agilite = expected_agilite
        # THEN
        self.assertEqual(expected_agilite, monstre._statistique.agilite)


    def test_get_agilite(self):
        # GIVEN
        expected_agilite = 10
        monstre = Monstre(agilite=expected_agilite)
        # WHEN
        # THEN
        self.assertEqual(monstre.agilite, monstre._statistique.agilite)


    def test_set_magie(self):
        # GIVEN
        monstre = Monstre()
        # WHEN
        expected_magie = 10
        monstre.magie = expected_magie
        # THEN
        self.assertEqual(expected_magie, monstre._statistique.magie)


    def test_get_magie(self):
        # GIVEN
        expected_magie = 10
        monstre = Monstre(magie=expected_magie)
        # WHEN
        # THEN
        self.assertEqual(monstre.magie, monstre._statistique.magie)

    def test_set_defense(self):
        # GIVEN
        monstre = Monstre()
        # WHEN
        expected_defense = 10
        monstre.defense = expected_defense
        # THEN
        self.assertEqual(expected_defense, monstre._statistique.defense)


    def test_get_defense(self):
        # GIVEN
        expected_defense = 10
        monstre = Monstre(agilite=expected_defense)
        # WHEN
        # THEN
        self.assertEqual(monstre.defense, monstre._statistique.defense)


    def test_set_points_de_vie(self):
        # GIVEN
        monstre = Monstre()
        # WHEN
        expected_points_de_vie = 10
        monstre.points_de_vie = expected_points_de_vie
        # THEN
        self.assertEqual(expected_points_de_vie, monstre._statistique.points_de_vie)


    def test_get_points_de_vie(self):
        # GIVEN
        expected_points_de_vie = 10
        monstre = Monstre(agilite=expected_points_de_vie)
        # WHEN
        # THEN
        self.assertEqual(monstre.points_de_vie, monstre._statistique.points_de_vie)


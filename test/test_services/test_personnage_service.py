from unittest import TestCase

from business_objects.attaque_info import AttaqueInfo
from business_objects.objet.arme.arc import Arc
from business_objects.objet.arme.baton import Baton
from business_objects.objet.arme.epee import Epee
from business_objects.objet.armure.armure_legere import ArmureLegere
from business_objects.objet.armure.armure_lourde import ArmureLourde
from business_objects.objet.armure.robe import Robe
from business_objects.personnage.guerrier import Guerrier
from business_objects.personnage.magicien import Magicien
from business_objects.personnage.voleur import Voleur
from service.personnage_service import PersonnageService


class TestPersonnageService(TestCase):
    def test_attaque_magicien_baton_en_bois(self):
        # GIVEN
        expected_output = "Le lourd bâton de bois s'abat sur le crâne de l'enmemi"
        baton_en_bois = Baton(
            nom="Baton en bois"
            , id=1
            , force_bonus=0
            , agilite_bonus=0
            , magie_bonus=0
            , defense_bonus=0
            , points_de_vie_bonus=0
            , description_attaque=expected_output
            , base_attaque=10)
        robe = Robe(
            nom="Robe"
            , force_bonus=0
            , agilite_bonus=0
            , magie_bonus=0
            , defense_bonus=0
            , points_de_vie_bonus=0
            , description_defense=""
            , base_defense=0)
        magicien = Magicien(
            force=1
            , agilite=1
            , magie=1
            , defense=1
            , points_de_vie=1
            , arme=baton_en_bois
            , armure=robe
        )

        # WHEN
        actual_output = PersonnageService.attaquer(magicien).phrase_attaque

        # THEN
        self.assertEqual(expected_output, actual_output)

    def test_attaque_guerrier_master_sword(self):
        # GIVEN
        expected_output = "En éclair lumineux s'abatit sur le monstre"
        master_sword = Epee(
            "Master sword"
            , force_bonus=0
            , agilite_bonus=0
            , magie_bonus=0
            , defense_bonus=0
            , points_de_vie_bonus=0
            , description_attaque=expected_output
            , base_attaque=750)
        harnois = ArmureLourde("Harnois complet"
                               , force_bonus=0
                               , agilite_bonus=0
                               , magie_bonus=0
                               , defense_bonus=0
                               , points_de_vie_bonus=0
                               , description_defense=""
                               , base_defense=0)

        guerrier = Guerrier(
            force=1
            , agilite=1
            , magie=1
            , defense=1
            , points_de_vie=1
            , arme=master_sword
            , armure=harnois
        )

        # WHEN
        actual_output = PersonnageService.attaquer(guerrier).phrase_attaque

        # THEN
        self.assertEqual(expected_output, actual_output)

    def test_attaque_voleur_arc_de_lynel(self):
        # GIVEN
        expected_output = "Trois flèches se fichèrent dans le crâne de " \
                          "l'adversaire"
        arc_de_lynel = Arc(
            "arc du dieu bestial"
            , force_bonus=0
            , agilite_bonus=0
            , magie_bonus=0
            , defense_bonus=0
            , points_de_vie_bonus=0
            , description_attaque=expected_output
            , base_attaque=10)
        armure_cuir = ArmureLegere("Armure de cuir", force_bonus=0
                                   , agilite_bonus=0
                                   , magie_bonus=0
                                   , defense_bonus=0
                                   , points_de_vie_bonus=0
                                   , description_defense=""
                                   , base_defense=0)
        voleur = Voleur(
            force=1
            , agilite=1
            , magie=1
            , defense=1
            , points_de_vie=1
            , arme=arc_de_lynel
            , armure=armure_cuir
        )

        # WHEN
        actual_output = PersonnageService.attaquer(voleur).phrase_attaque

        # THEN
        self.assertEqual(expected_output, actual_output)

    def test_defense_magicien_robe(self):
        # GIVEN
        expected_output = "Le tissus reforcé par magie protégea son porteur"
        baton_en_bois = Baton(
            "Baton en bois"
            , force_bonus=0
            , agilite_bonus=0
            , magie_bonus=0
            , defense_bonus=0
            , points_de_vie_bonus=0
            , description_attaque=""
            , base_attaque=10)
        robe = Robe("Robe", force_bonus=0
                    , agilite_bonus=0
                    , magie_bonus=0
                    , defense_bonus=0
                    , points_de_vie_bonus=0
                    , description_defense=expected_output
                    , base_defense=0)
        magicien = Magicien(
            force=1
            , agilite=1
            , magie=1
            , defense=1
            , points_de_vie=1
            , arme=baton_en_bois
            , armure=robe
        )

        info_attaque = AttaqueInfo("", 0)

        # WHEN
        actual_output = PersonnageService.defendre(magicien,
                                                  info_attaque).phrase_defense

        # THEN
        self.assertEqual(expected_output, actual_output)

    def test_defense_guerrier_harnois(self):
        # GIVEN
        expected_output = "L'imposante armure absorba l'attaque"
        master_sword = Epee(
            "Master sword"
            , force_bonus=0
            , agilite_bonus=0
            , magie_bonus=0
            , defense_bonus=0
            , points_de_vie_bonus=0
            , description_attaque=""
            , base_attaque=750)
        harnois = ArmureLourde("Harnois complet", force_bonus=0
                               , agilite_bonus=0
                               , magie_bonus=0
                               , defense_bonus=0
                               , points_de_vie_bonus=0
                               , description_defense=expected_output
                               , base_defense=0)
        guerrier = Guerrier(
            force=1
            , agilite=1
            , magie=1
            , defense=1
            , points_de_vie=1
            , arme=master_sword
            , armure=harnois
        )

        info_attaque = AttaqueInfo("", 0)

        # WHEN
        actual_output = PersonnageService.defendre(guerrier,
                                                  info_attaque).phrase_defense

        # THEN
        self.assertEqual(expected_output, actual_output)

    def test_defense_voleur_armure_de_cuir(self):
        # GIVEN
        expected_output = "Le cuir tint bon et limita les dégâts"
        arc_de_lynel = Arc(
            "Arc de Lynel"
            , force_bonus=0
            , agilite_bonus=0
            , magie_bonus=0
            , defense_bonus=0
            , points_de_vie_bonus=0
            , description_attaque=""
            , base_attaque=10)
        armure_cuir = ArmureLegere("Armure de cuir", force_bonus=0
                                   , agilite_bonus=0
                                   , magie_bonus=0
                                   , defense_bonus=0
                                   , points_de_vie_bonus=0
                                   , description_defense=expected_output
                                   , base_defense=0)
        voleur = Voleur(
            force=1
            , agilite=1
            , magie=1
            , defense=1
            , points_de_vie=1
            , arme=arc_de_lynel
            , armure=armure_cuir
        )

        info_attaque = AttaqueInfo("", 0)

        # WHEN
        actual_output = PersonnageService.defendre(voleur,
                                                  info_attaque).phrase_defense

        # THEN
        self.assertEqual(expected_output, actual_output)

from unittest import TestCase

from business_objects.objet.arme.arme import Arme
from business_objects.personnage.magicien import Magicien
from exceptions.arme_interdite_exception import ArmeInterditeException
from services.personnage_service import PersonnageService


class TestMagicien(TestCase):


    def test_arme_baton_de_feu_magicien(self):
        # GIVEN
        expected_output = "Lance des boules de feu"
        arme = Arme(
            nom="Baton de feu"
            , force_bonus=0
            , agilite_bonus=0
            , magie_bonus=0
            , defense_bonus=0
            , point_de_vie_bonus=0
            , base_attaque=0)
        magicien = Magicien(
            force=0
            , agilite=0
            , magie=0
            , defense=0
            , points_de_vie=0
            , arme=arme)
        # WHEN
        actual_output = PersonnageService.attaquer(magicien).phrase_attaque
        # THEN
        self.assertEqual(expected_output, actual_output)


    def test_arme_baton_de_glace_magicien(self):
        # GIVEN
        expected_output = "Fait tomber des pic de glace"
        arme = Arme(
            nom="Baton de glace"
            , force_bonus=0
            , agilite_bonus=0
            , magie_bonus=0
            , defense_bonus=0
            , point_de_vie_bonus=0
            , base_attaque=0)
        magicien = Magicien(0, 0, 0, 0, 0, arme)
        # WHEN
        actual_output = PersonnageService.attaquer(magicien).phrase_attaque
        # THEN
        self.assertEqual(expected_output, actual_output)


    def test_arme_necronomicon_magicien(self):
        # GIVEN
        expected_output = "Invoque un Grand Ancien"
        arme = Arme(
            nom="Necronomicon"
            , force_bonus=0
            , agilite_bonus=0
            , magie_bonus=0
            , defense_bonus=0
            , point_de_vie_bonus=0
            , base_attaque=0)
        magicien = Magicien(0, 0, 0, 0, 0, arme)
        # WHEN
        actual_output = PersonnageService.attaquer(magicien).phrase_attaque
        # THEN
        self.assertEqual(expected_output, actual_output)


    def test_ArmeInterditeException_magicien(self):
        # GIVEN
        arme = Arme("", 0, 0, 0, 0, 0, 0)
        magicien = Magicien(0, 0, 0, 0, 0, arme)
        # WHEN
        # THEN
        with self.assertRaises(ArmeInterditeException):
            PersonnageService.attaquer(magicien)

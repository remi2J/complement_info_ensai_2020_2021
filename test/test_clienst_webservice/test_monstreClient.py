from random import randint
from unittest import TestCase

from business_objects.element.monstre import Monstre
from client.monstre_client import MonstreClient


class TestMonstreClient(TestCase):
    def test_get_liste_monstres(self):
        # GIVEN

        # WHEN
        liste_monstre = MonstreClient.get_liste_monstres()
        # THEN
        self.assertIsNotNone(liste_monstre)

    def test_get_liste_monstres(self):
        # GIVEN

        # WHEN
        liste_monstre = MonstreClient.get_liste_monstres_from_xml()
        # THEN
        self.assertIsNotNone(liste_monstre)

    def test_create_monstre(self):
        #GIVEN
        random = randint(1,10000)
        freezer = Monstre(
            nom="Freezer_test"+str(random),
            description="Tyran galactique, Freezer est redouté de tous ses "
                        "sujets. Combattant redoutable,beau,totalement dénué "
                        "de scrupules,impitoyable, cruel, il a sous ses "
                        "ordres une armée de soldats qu'il utilise pour "
                        "conquérir des planètes afin de les revendre ",
            type_monstre="humanoïde",
            points_de_vie=9999999999,
            force=99999999,
            magie=99999999,
            agilite=9999999,
            defense=9999999,
        )
        #WHEN

        est_poste = MonstreClient.create_monstre(freezer)
        MonstreClient.delete_monstre(freezer)

        #THEN
        self.assertTrue(est_poste)


    def test_update_monstre(self):
        #GIVEN
        random = randint(1,10000)
        freezer = Monstre(
            nom="Freezer_test"+str(random),
            description="Tyran galactique, Freezer est redouté de tous ses "
                        "sujets. Combattant redoutable,beau,totalement dénué "
                        "de scrupules,impitoyable, cruel, il a sous ses "
                        "ordres une armée de soldats qu'il utilise pour "
                        "conquérir des planètes afin de les revendre ",
            type_monstre="humanoïde",
            points_de_vie=9999999999,
            force=99999999,
            magie=99999999,
            agilite=9999999,
            defense=9999999,
        )
        #WHEN

        MonstreClient.create_monstre(freezer)
        freezer.force=0
        est_update = MonstreClient.update_monstre(freezer)

        MonstreClient.delete_monstre(freezer)

        #THEN
        self.assertTrue(est_update)

    def test_delete_monstre(self):
        #GIVEN
        random = randint(1,10000)
        freezer = Monstre(
            nom="Freezer_test"+str(random),
            description="Tyran galactique, Freezer est redouté de tous ses "
                        "sujets. Combattant redoutable,beau,totalement dénué "
                        "de scrupules,impitoyable, cruel, il a sous ses "
                        "ordres une armée de soldats qu'il utilise pour "
                        "conquérir des planètes afin de les revendre ",
            type_monstre="humanoïde",
            points_de_vie=9999999999,
            force=99999999,
            magie=99999999,
            agilite=9999999,
            defense=9999999,
        )
        #WHEN

        MonstreClient.create_monstre(freezer)
        est_supprime = MonstreClient.delete_monstre(freezer)

        #THEN
        self.assertTrue(est_supprime)
from unittest import TestCase
from unittest.mock import patch

from business_objects.monstre.monstre import Monstre
from business_objects.monstre.type_monstre import TypeMonstre
from service.monstre_service import MonstreService


class TestMonsterService(TestCase):

    @patch('dao.monster_dao.MonsterDao.find_by_name')
    def test_get_monster_from_db_by_name(self, mock_find_by_name):
        # GIVEN
        mock_find_by_name.return_value = Monstre(
            id=1
            , nom="test"
            , description="description test"
            , id_type_monstre=1
            , label_type_monstre="type test"
            , force=10
            , agilite=10
            , magie=10
            , defense=10
            , points_de_vie=10)

        # WHEN
        monstre = MonstreService.get_monster_from_db_by_name("test")

        # THEN
        self.assertIsNotNone(monstre.id)
        self.assertEqual("test", monstre.nom)

    @patch('dao.type_monstre_dao.TypeMonstreDao.find_by_name')
    def test_build_monster(self, mock_find_by_name):
        # GIVEN
        mock_find_by_name.return_value = TypeMonstre(id_type=1, label="test")
        attributs = {
            "nom": "test",
            "description": "test",
            "force": 10,
            "agilite": 10,
            "magie": 10,
            "defense": 10,
            "points_de_vie": 10,
            "type_monstre": 1
        }
        # WHEN
        monster = MonstreService.build_monster(**attributs)
        # THEN
        self.assertEqual(attributs["nom"], monster.nom)
        self.assertEqual(attributs["description"], monster.description)
        self.assertEqual(attributs["force"], monster.force)
        self.assertEqual(attributs["agilite"], monster.agilite)
        self.assertEqual(attributs["magie"], monster.magie)
        self.assertEqual(attributs["defense"], monster.defense)
        self.assertEqual(attributs["points_de_vie"], monster.points_de_vie)
        self.assertEqual(attributs["type_monstre"], monster.type_monstre.id_type)
        self.assertEqual("test", monster.type_monstre.__label)

from lxml import etree
import requests

from business_objects.element.monstre import Monstre
from configuration import properties


class MonstreClient():
    """
    Permet de se connecter au webservice pour générer des monstres
    """

    RESOURCE_MONSTRES_PATH= "/monsters"

    @staticmethod
    def get_liste_monstres():
        """
        Fait un appel au webservice pour obtenir la liste des monstres
        accessible. Les instances retournées ne sont pas complète, il manque
        leur statistiques
        :return: La liste des monstres accessibles
        :rtype: list
        """
        requete = requests.get(
            properties.host_ws+MonstreClient.RESOURCE_MONSTRES_PATH)
        output_monstres = []
        if requete.status_code == 200 :
            response_json = requete.json()
            for monstre_json in response_json["result"]:
                # on itére sur les résultats
                output_monstres.append(Monstre(nom=monstre_json["_nom"]
                                               , description=monstre_json["__description"]
                                               , type_monstre=monstre_json["__type_monstre"]))
        return output_monstres

    @staticmethod
    def get_liste_monstres_from_xml():
        """
        Fait un appel au webservice pour obtenir la liste des monstres
        accessible. Les instances retournées ne sont pas complète, il manque
        leur statistiques
        :return: La liste des monstres accessibles
        :rtype: list
        """
        headers = {"accept": "application/xml"}
        requete = requests.get(properties.host_ws+MonstreClient.RESOURCE_MONSTRES_PATH, headers=headers)
        output_monstres = []
        if requete.status_code == 200:
            reponse_xml= etree.XML(requete.text)



            for monstre_xml in reponse_xml.xpath('/response/result') :
                # on itére sur les résultats
                output_monstres.append(Monstre(nom=monstre_xml.findtext("_nom")
                                               ,
                                               description=monstre_xml.findtext("__description")
                                               ,
                                               type_monstre=monstre_xml.findtext("__type_monstre")))
        return output_monstres

    @staticmethod
    def get_all_info_form_monstre(monstre):
        """
        Méthode pour récupérer l'intégralité des informations d'un monstre à
        partir du monstre passé en paramètre. On va mettre à jour le monstre en
        paramètre. Il ne sera pas retourné, juste mis à jour
        :param monstre : le montre que l'on veut complèter
        :type monstre : Monstre
        """
        requete = requests.get(properties.host_ws + MonstreClient.RESOURCE_MONSTRE_PATH + monstre._nom)
        if requete.status_code == 200:
            requete_json = requete.json()
            monstre.__description = requete_json["__description"]
            monstre.__type_monstre = requete_json["__type_monstre"]
            monstre.points_de_vie = requete_json["points_de_vie"]
            monstre.force = requete_json["force"]
            monstre.magie = requete_json["magie"]
            monstre.agilite = requete_json["agilite"]
            monstre.defense = requete_json["defense"]

    @staticmethod
    def create_monstre(monstre):
        """
        Envoie les informations d'un monstre au web service pour l'ajouter en base
        :param monstre: le monstre à ajouer
        :type monstre: Monstre
        :return: Si l'insertion c'est bien passé
        :rtype: bool
        """
        #Creation du dictionnaire
        monstre_dict = {
            "nom": monstre.nom,
            "description": monstre.description,
            "type_monstre": monstre.type_monstre,
            "points_de_vie": monstre.points_de_vie,
            "force": monstre.force,
            "magie": monstre.magie,
            "agilite": monstre.agilite,
            "defense": monstre.defense
        }
        requete = requests.put(properties.host_ws +
                             MonstreClient.RESOURCE_MONSTRES_PATH +"/"+
                                monstre.nom,
                                json=monstre_dict)

        requete_ok = False
        if requete.status_code == 201 or requete.status_code == 200:
            requete_ok = True
        return requete_ok

    @staticmethod
    def update_monstre(monstre):
        """
        Envoie les informations d'un monstre au web service pour modifier le
        monstre existant
        :param monstre: le monstre à modifier
        :type monstre: Monstre
        :return: si la mise à jour à eu lieu
        :rtype: bool
        """

        #Creation du dictionnaire
        monstre_dict = {
            "nom": monstre.nom,
            "description": monstre.description,
            "type_monstre": monstre.type_monstre,
            "points_de_vie": monstre.points_de_vie,
            "force": monstre.force,
            "magie": monstre.magie,
            "agilite": monstre.agilite,
            "defense": monstre.defense
        }
        requete = requests.post(properties.host_ws +
                             MonstreClient.RESOURCE_MONSTRES_PATH +"/"+
                                monstre.nom,
                                json=monstre_dict)

        requete_ok = False
        if requete.status_code == 201 or requete.status_code == 200:
            requete_ok = True
        return requete_ok

    @staticmethod
    def delete_monstre(monstre):
        """
        Envoie les informations d'un monstre au web service pour supprimer le
        monstre existant
        :param monstre: le monstre à supprimer
        :type monstre: Monstre
        :return: si la suppression à eu lieu
        :rtype: bool
        """

        requete = requests.delete(properties.host_ws +
                                MonstreClient.RESOURCE_MONSTRES_PATH + "/" +
                                monstre.nom)

        requete_ok = False
        if requete.status_code == 201 or requete.status_code == 200:
            requete_ok = True
        return requete_ok
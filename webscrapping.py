import requests
from lxml import etree


if __name__ == "__main__":
    requete = requests.get("https://fr.wikipedia.org/wiki/Web_scraping")
    titles_content = {}

    #génération de tous les tag recherchés
    header_tags = ["h"+str(i) for i in range(1,7)]

    # Si on a un code 200 c'est que la requête est ok
    if requete.status_code == 200:
        #conversion en arbre XML
        reponse_xml = etree.XML(requete.content)

        # On itère sur les tags
        for tag in header_tags:
            titles_content[tag] = []
            for header in reponse_xml.xpath('//'+tag) :
                titles_content[tag].append(etree.tostring(header))

    print(titles_content)
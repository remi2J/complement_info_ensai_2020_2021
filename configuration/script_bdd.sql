DROP SEQUENCE IF EXISTS  id_objet CASCADE;
CREATE SEQUENCE id_objet;

DROP TABLE IF EXISTS type_arme CASCADE;
CREATE TABLE type_arme
(
    type_arme_id serial,
    type_arme_label text COLLATE pg_catalog."default" UNIQUE,
    CONSTRAINT type_arme_pkey PRIMARY KEY (type_arme_id)
);

INSERT INTO type_arme (type_arme_id, type_arme_label) VALUES
(1,'arc'),
(2,'baton'),
(3,'couteau'),
(4,'epee'),
(5,'lance'),
(6,'livre');

DROP TABLE IF EXISTS statistique_generale CASCADE;
CREATE TABLE statistique_generale (
    id_statistique serial ,
    force integer,
    agilite integer,
    magie integer,
    defense integer,
    points_de_vie integer,
    CONSTRAINT arme_statistique_generale PRIMARY KEY (id_statistique)
);

DROP TABLE IF EXISTS arme CASCADE;
CREATE TABLE arme
(
    id_arme integer NOT NULL DEFAULT nextval('id_objet'),
    id_label integer,
    nom text COLLATE pg_catalog."default",
    description_attaque text COLLATE pg_catalog."default",
    base_attaque integer,
    CONSTRAINT arme_pkey PRIMARY KEY (id_arme),
    CONSTRAINT arme_label_fkey FOREIGN KEY (id_label) REFERENCES type_arme(type_arme_id)
)INHERITS (statistique_generale);

DROP TYPE IF EXISTS type_perso;
CREATE TYPE type_perso AS ENUM ('guerrier', 'voleur', 'magicien');

DROP TABLE IF EXISTS personnage CASCADE;
CREATE TABLE personnage (
    id_personnage serial,
    id_arme integer,
    nom text,
    type_personnage type_perso,
    CONSTRAINT arme_statistique_personnage PRIMARY KEY (id_personnage)
)INHERITS (statistique_generale);



INSERT INTO arme (id_arme, id_label, nom, description_attaque, base_attaque,
force, agilite, magie, defense, points_de_vie) VALUES
(1,4, 'master sword', 'Un eclair de lumière pourfend l''adversaire', 100,35,10,10,15,50)
, (2, 5, 'trident de Poseidon', 'Un raz-de-marée noie l''adversaire', 150,60,0,42,12,50);


INSERT INTO personnage (nom,type_personnage,id_arme, force, agilite, magie,
defense, points_de_vie) VALUES
('Link','guerrier',1, 120,100,10,150,654),
('Aragorn','guerrier',1, 320,20,0,1500,500),
('Cyric','voleur',1, 120,1000,10,150,547),
('Arsène Lupin','voleur',1, 52,168,10,150,764),
('Kaito Kid','voleur',1, 120,100,10,150,15),
('Gandalf', 'magicien',1, 453,100,568,534,645),
('Merlin','magicien',2,30,765,98,457,4536);



DROP TABLE IF EXISTS statistique_monstre CASCADE;
DROP TABLE IF EXISTS monstre CASCADE;
DROP TABLE IF EXISTS type_monstre CASCADE;

DROP SEQUENCE IF EXISTS id_monstre_seq ;
DROP SEQUENCE IF EXISTS id_type_monstre_seq ;
CREATE SEQUENCE id_monstre_seq ;
CREATE SEQUENCE id_type_monstre_seq ;


CREATE TABLE type_monstre (
	id_type_monstre integer NOT NULL DEFAULT nextval
	('id_type_monstre_seq'::regclass) PRIMARY KEY,
	label_type_monstre text
);

INSERT INTO type_monstre (id_type_monstre, label_type_monstre) VALUES
(1, 'aberration'),
(2, 'humanoïde'),
(3,'animal'),
(4,'dragon'),
(5,'créature magique'),
(6,'mort-vivant'),
(7,'créature artificielle'),
(8, 'plante'),
(9, 'extérieur'),
(10, 'vase');

CREATE TABLE monstre (
	id_monstre integer NOT NULL DEFAULT nextval
	('id_monstre_seq'::regclass),
    nom text COLLATE pg_catalog."default" NOT NULL,
    description text COLLATE pg_catalog."default",
    type_monstre integer,
    CONSTRAINT monstre_pkey PRIMARY KEY (id_monstre),
    CONSTRAINT monstre_nom_key UNIQUE (nom),
    CONSTRAINT monstre_type_monstre_fkey FOREIGN KEY (type_monstre)
        REFERENCES public.type_monstre (id_type_monstre) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)INHERITS (statistique_generale);

INSERT INTO monstre (id_monstre,nom, description, type_monstre, force,
agilite, magie, defense, points_de_vie) VALUES
(1,'grizzly', 'Les puissants muscles qu’on voit bouger sous la fourrure brune de cet ours imposant laissent présager de sa rapidité et de sa force.', 2,60,0,20,50,1500),
(2,'objet animé', 'Les os enfermés dans la cage s’entrechoquent quand sesjambes de chaînes s’animent et qu’elle part en quête de nouveaux prisonniers.', 7,30,0,10,20,400),
(3,'farfadet','Ce petit humanoïde aux oreilles pointues, aux yeux verts etau sourire sadique. Il tient une bouteille dans une main et un gourdin dans l’autre.', 5,5,30,30,20,300),
(4,'fleur de lune','Une gueule grand ouverte, prête à avaler une victime, setient au sommet d’un tronc difforme parsemé de bourgeons boursouflés.',8,15,15,15,15,300),
(5,'planétar','Ce grand humanoïde musclé et chauve possède une peau couleurd’émeraude et deux paires d’ailes aux plumes brillantes.',9,100,100,50,100,3000),
(6,'gobelin','La large tête disgracieuse de cet humanoïde d’à peine un mètresemble totalement disproportionnée par rapport à son corps maigrelet.',2,20,0,10,20,200),
(7,'babau','Cette créature émaciée ressemble à un squelette d’humain cornurecouvert d’une fine peau de cuir huileux qui lui colle aux os.',9,40,50,40,30,800),
(8,'hydre','Plusieurs têtes de serpent visiblement fâchées s’élèvent du corps serpentin de ce monstre terrifiant.',5,70,0,20,80,5000),
(9,'couleur tombée du ciel','Une étrange lueur, semblable à aucune autre,illumine soudain les lieux, apportant avec elle une impression étouffante de malveillance latente.',10,50,30,0,45,1250),
(10,'jorôgumo','Huit grandes pattes d’araignée toutes grêles, couvertes detouffes de poils noirs épais, sortent du dos de cette femme aux cheveux noirs, par ailleurs très jolie.',2,100,30,100,80,600),
(11,'cristal carnivore','Les facettes de cette formation cristalline semodifient et vibrent, comme en signe d’anticipation.',10,20,50,20,100,450),
(12,'cthulhu','Cette gigantesque impossibilité, qui n’est ni pieuvre nidragon ni un géant mais quelque chose de bien pire, doit sûrement annoncer la fin des temps.',1,800,800,800,800,10000),
(13,'tarasque','Comme un dinosaure, ce gigantesque reptile domine tout ce qui l’entoure. Il n’est que dents, cornes, griffes et queue épineuse.',5,1500,1500,300,1500,20000),
(14,'licorne','Cette magnifique bête semblable à un cheval blanc a unebarbiche de chèvre et une unique et longue corne d’ivoire sur le front.',5,60,150,50,50,400),
(15,'dévoreur d''intellect','Le corps de cette créature sans tête ressemble à un gros cerveau gluant possédant quatre petites pattes griffues pour seuls membres.',1,30,100,80,50,163),
(16,'mage mécanique','Cette créature artificielle sans visage est dotéed’une baguette de cristal incrustée dans la poitrine. Elle crépite d’énergie magique.',7,50,150,30,80,480),
(17,'yuki-onna','Cette belle femme au visage triste porte une robe ornée dejolis motifs et elle est entourée d’une masse de neige tourbillonnante.',6,80,120,180,65,190),
(18,'revenant','Un cadavre déformé et mutilé s’avance lourdement. Il tend ses doigts osseux et affûtés. Ses intentions maléfiques sont évidentes.',6,50,0,30,80,1000),
(19,'dragon impérial du ciel','Pourtant dépourvu d’ailes, ce dragon sillonne le ciel en serpentant avec grâce, ses écailles réfléchissant les nuances changeantes des cieux.',4,600,600,600,600,7000);


ALTER SEQUENCE id_monstre_seq RESTART WITH 20;
ALTER SEQUENCE id_type_monstre_seq RESTART WITH 11;




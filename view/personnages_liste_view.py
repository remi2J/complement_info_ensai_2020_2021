from PyInquirer import prompt, Separator

from service.personnage_service import PersonnageService
from view.abstract_vue import AbstractView


class PersonnagesListeView(AbstractView):

    def __init__(self):
        # la liste des personnes que l'on affiche
        self.personnages_affiches = \
            PersonnageService.get_all_personnage_from_db();
        # la liste avec uniquement les noms
        self.personnage_nom = [personnage.nom for personnage in
                 self.personnages_affiches]

        # Création du menu
        choix_personnage = self.personnage_nom
        choix_personnage.append(Separator())
        choix_personnage.append("Retour menu")
        self.questions = [
            {
                'type': 'list',
                'name': 'personnages',
                'message': 'Veillez choisir un personnage :',
                'choices': choix_personnage
            }
        ]

    def display_info(self):
        pass

    def make_choice(self):
        reponse = prompt(self.questions)
        AbstractView.session.personnage_actif = reponse["personnages"]
        if reponse["personnages"] == "Retour menu":
            from view.welcome_view import WelcomeView
            return WelcomeView
        else:
            # Besoin de récupérer le pesonnaga avec son nom. On va aller vite
            # et se baser sur les index.
            index = self.personnage_nom.index(reponse["personnages"])
            AbstractView.session.personnage_actif =  \
                self.personnages_affiches[index]
            from view.personnage_view import PersonnageView
            return PersonnageView()

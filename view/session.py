from business_objects.monstre.monstre import Monstre
from business_objects.personnage.abstract_personnage import AbstractPersonnage


class Session:
    def __init__(self):
        """
        Définition des variables que l'on stocke en session
        Le syntaxe
        ref:type = valeur
        permet de donner le type des variables. Utile pour l'autocompletion.
        """
        self.user_name: str = "Bob"
        self.user_mdp: str = None
        self.personnage_actif: AbstractPersonnage = None
        self.monstre_actif: Monstre = None
        pass

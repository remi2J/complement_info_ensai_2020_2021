from PyInquirer import Separator, prompt

from view.abstract_vue import AbstractView


class PersonnageView(AbstractView):
    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'menu',
                'message': 'Que voulez vous faire ? :',
                'choices': [
                    'liste personnage',
                    Separator(),
                    'Menu Principal'
                ]
            }
        ]

    def display_info(self):
        print("Nom du personnage : {}".format(AbstractView.session.personnage_actif.nom))
        print("Force : {}".format(
            AbstractView.session.personnage_actif.statistique.force))
        print("Agilite: {}".format(
            AbstractView.session.personnage_actif.statistique.agilite))
        print("Magie : {}".format(
            AbstractView.session.personnage_actif.statistique.magie))
        print("Defense : {}".format(
            AbstractView.session.personnage_actif.statistique.defense))
        print("Points de vie : {}".format(
            AbstractView.session.personnage_actif.statistique.points_de_vie))


    def make_choice(self):
        reponse = prompt(self.questions)
        if reponse["menu"]=='liste personnage':
            from view.personnages_liste_view import PersonnagesListeView
            return PersonnagesListeView()
        else :
            from view.welcome_view import WelcomeView
            return WelcomeView()

from PyInquirer import prompt, Separator

from service.monstre_service import MonstreService
from service.personnage_service import PersonnageService
from view.abstract_vue import AbstractView
from view.monstre_view import MonstreView


class MonstresListeView(AbstractView):

    def __init__(self):
        # la liste des monstre que l'on affiche
        self.monstres_affiches = \
            MonstreService.get_all_monstres_from_db();
        # la liste avec uniquement les noms
        self.montres_nom = [monstre.nom for monstre in
                 self.monstres_affiches]

        # Création du menu
        choix_montre = self.montres_nom
        choix_montre.append(Separator())
        choix_montre.append("Retour menu")

        self.questions = [
            {
                'type': 'list',
                'name': 'monstres',
                'message': 'Veillez choisir un monstre :',
                'choices': choix_montre
            }
        ]

    def display_info(self):
        pass

    def make_choice(self):
        reponse = prompt(self.questions)
        AbstractView.session.personnage_actif = reponse["monstres"]
        if reponse["monstres"] == "Retour menu":
            from view.welcome_view import WelcomeView
            return WelcomeView
        else:
            # Besoin de récupérer le pesonnaga avec son nom. On va aller vite
            # et se baser sur les index.
            index = self.montres_nom.index(reponse["monstres"])
            AbstractView.session.monstre_actif =  \
                self.monstres_affiches[index]
            return MonstreView()

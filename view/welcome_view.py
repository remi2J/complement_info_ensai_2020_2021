from PyInquirer import prompt

from view.abstract_vue import AbstractView


class WelcomeView(AbstractView):

    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'choix_menu',
                'message': 'Menu',
                'choices': [
                    "liste personnages",
                    "liste monstres"
                ]
            }
        ]

    def make_choice(self):
        reponse = prompt(self.questions)
        if reponse['choix_menu'] == 'liste personnages':
            from view.personnages_liste_view import PersonnagesListeView
            return PersonnagesListeView()
        if reponse["choix_menu"] == 'liste monstres':
            from view.montres_liste_view import MonstresListeView
            return MonstresListeView()

    def display_info(self):
        print('Bienvenue {}, content de vous savoir parmi nous !'.format(
            AbstractView.session.user_name))

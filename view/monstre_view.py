from PyInquirer import Separator, prompt

from view.abstract_vue import AbstractView


class MonstreView(AbstractView):
    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'menu',
                'message': 'Que voulez vous faire ? :',
                'choices': [
                    'liste monstres',
                    Separator(),
                    'Menu Principal'
                ]
            }
        ]

    def display_info(self):
        print("Nom du monstre : {}".format(
            AbstractView.session.monstre_actif.nom))
        print("Force : {}".format(
            AbstractView.session.monstre_actif.force))
        print("Agilite: {}".format(
            AbstractView.session.monstre_actif.agilite))
        print("Magie : {}".format(
            AbstractView.session.monstre_actif.magie))
        print("Defense : {}".format(
            AbstractView.session.monstre_actif.defense))
        print("Points de vie : {}".format(
            AbstractView.session.monstre_actif.points_de_vie))
        print("descripion: {}".format(
            AbstractView.session.monstre_actif.description))
        print("Type : {}".format(
            AbstractView.session.monstre_actif.type_monstre.label))

        
    def make_choice(self):
        reponse = prompt(self.questions)
        if reponse["menu"] == 'liste monstres':
            from view.montres_liste_view import MonstresListeView
            return MonstresListeView()
        else:
            from view.welcome_view import WelcomeView
            return WelcomeView()

import dotenv
from dotenv import load_dotenv
from flask import Flask, make_response
from flask_restful import  Api
from simplexml import dumps

from configuration import properties
from configuration.properties import url_monster, url_arme
from resources.arme_list_resource import ArmeListResource
from resources.arme_resource import ArmeResource
from resources.monster_resource import MonsterResource
from resources.monstres_list_resource import MonsterListResource

# Import variable env
load_dotenv()
app = Flask(__name__)


api = Api(app, default_mediatype='application/json')

api.add_resource(MonsterListResource, url_monster)
api.add_resource(MonsterResource, url_monster+'/<string:monster_name>')

api.add_resource(ArmeListResource, url_arme)
api.add_resource(ArmeResource, url_arme+'/<string:nom_arme>')


@api.representation('application/xml')
def output_xml(data, code, headers=None):
    """Makes a Flask response with a XML encoded body"""
    resp = make_response(dumps({'response':data}), code)
    resp.headers.extend(headers or {})
    return resp

if __name__ == '__main__':
    app.run(debug=True)
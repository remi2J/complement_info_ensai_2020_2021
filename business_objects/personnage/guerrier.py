from business_objects.objet.arme.arc import Arc
from business_objects.objet.arme.couteau import Couteau
from business_objects.objet.arme.epee import Epee
from business_objects.objet.arme.lance import Lance
from business_objects.personnage.abstract_personnage import AbstractPersonnage


class Guerrier(AbstractPersonnage):
    ARMES_AUTORISEES = [Epee, Arc, Lance, Couteau]
    NOM_BASE_DE_DONNEE = "guerrier"

    def __init__(self,nom, force, agilite, magie, defense,
                 points_de_vie, arme=None, armure=None,id=None):
        super().__init__(
            nom=nom,
            force=force,
            agilite=agilite,
            magie=magie,
            defense=defense,
            points_de_vie=points_de_vie,
            nom_base_de_donnee=Guerrier.NOM_BASE_DE_DONNEE,
            arme=arme,
            armure=armure,
            id=id)

    def degats_attaque(self):
        # code spécifique au guerrier ici
        return self.arme.utiliser_arme(
            self.statistique_finale) * self.statistique_finale.force * 0.8

    def reduction_degats(self, degats_subis):
        # code spécifique au guerrier ici
        return self.armure.utiliser_armure(self.statistique_finale, degats_subis)

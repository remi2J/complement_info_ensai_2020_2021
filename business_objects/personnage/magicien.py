from business_objects.objet.arme.baton import Baton
from business_objects.objet.arme.livre import Livre
from business_objects.personnage.abstract_personnage import AbstractPersonnage


class Magicien(AbstractPersonnage):
    ARMES_AUTORISEES = [Livre, Baton]
    NOM_BASE_DE_DONNEE = "magicien"

    def __init__(self, nom, force, agilite, magie, defense,
                 points_de_vie, arme=None, armure=None, id=None):
        super().__init__(
            nom=nom,
            force=force,
            agilite=agilite,
            magie=magie,
            defense=defense,
            points_de_vie=points_de_vie,
            nom_base_de_donnee=Magicien.NOM_BASE_DE_DONNEE,
            arme=arme,
            armure=armure,
            id=id)

    def degats_attaque(self):
        # code spécifique au magicien ici
        return self.arme.utiliser_arme(
            self.statistique_finale) * self.statistique_finale.magie * 0.6

    def reduction_degats(self, degats_subis):
        # code spécifique au magicien ici
        return self.armure.utiliser_armure(self.statistique_finale,
                                           degats_subis)

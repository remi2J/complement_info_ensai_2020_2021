from business_objects.objet.arme.arc import Arc
from business_objects.objet.arme.couteau import Couteau
from business_objects.personnage.abstract_personnage import AbstractPersonnage


class Voleur(AbstractPersonnage):
    ARMES_AUTORISEES = [Arc, Couteau]
    NOM_BASE_DE_DONNEE = "voleur"

    def __init__(self,nom, force, agilite, magie, defense,
                 points_de_vie, arme=None, armure=None,id=None):
        super().__init__(
            nom=nom,
            force=force,
            agilite=agilite,
            magie=magie,
            defense=defense,
            points_de_vie=points_de_vie,
            nom_base_de_donnee=Voleur.NOM_BASE_DE_DONNEE,
            arme=arme,
            armure=armure,
            id=id)
    def degats_attaque(self):
        # code spécifique au voleur ici
        return self.arme.utiliser_arme(
            self.statistique_finale)*(self.statistique_finale.agilite*0.4 +
                                      self.statistique_finale.force*0.2)

    def reduction_degats(self, degats_subis):
        # code spécifique au voleur ici
        return self.armure.utiliser_armure(self.statistique_finale, degats_subis)

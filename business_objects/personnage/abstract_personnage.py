from abc import ABC, abstractmethod

from business_objects.attaque_info import AttaqueInfo
from business_objects.statistique import Statistique
from business_objects.objet.armure.abstract_armure import AbstractArmure
from business_objects.objet.arme.abstract_arme import AbstractArme


class AbstractPersonnage(ABC):
    def __init__(self, nom, force, agilite, magie, defense, \
                 points_de_vie, nom_base_de_donnee, arme=None, armure=
                 None, id=None):
        """
        Constructeur commun à tous les personnages
        :param arme: l'personnage du personnage
        :type arme: AbstractArme
        :param armure: l'armure du personnage
        :type armure: AbstractArmure
        :param force: la force du personnage
        :type force: int
        :param agilite: l'agilite du personnage
        :type agilite: int
        :param magie: la magie du personnage
        :type magie: int
        :param defense: la défense du personnage
        :type defense: int
        :param points_de_vie: les point de vie du personnage
        :type points_de_vie: int
        """
        self.__nom = nom
        self.NOM_BASE_DE_DONNEE = nom_base_de_donnee
        self.__statistique = Statistique(force=force,
                                         agilite=agilite,
                                         magie=magie,
                                         defense=defense,
                                         points_de_vie=points_de_vie)
        self._arme = arme
        self._armure = armure
        self._id = id

    @abstractmethod  # décorateur qui définit une méthode comme abstraite
    def degats_attaque(self):
        """
        Définit le comportement d'une attaque. Doit être implémenté par
        toutes  les classe qui héritent de personnage
        :return: le nombre de dégât de l'attaque
        :rtype: int
        """

    @abstractmethod  # décorateur qui définit une méthode comme abstraite
    def reduction_degats(self, degats_subis):
        """
        Définit la défense d'un personnage Doit être implémenté par toutes les
        classe qui héritent de personnage
        :param degats_subis: le nombre de dégât que va subir le personnage
        :type degats_subis: int
        :return: le nombre de dégât prévenu
        :rtype: int
        """

    @property
    def nom(self):
        return self.__nom

    @nom.setter
    def nom(self, nom):
        self.__nom = nom

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, new_id):
        self._id = new_id

    @property
    def statistique(self):
        return self.__statistique

    @property
    def arme(self):
        return self._arme

    @arme.setter
    def arme(self, arme):
        self._arme = arme

    @property
    def armure(self):
        return self._armure

    @armure.setter
    def armure(self, armure):
        self._armure = armure

    @property
    def statistique_finale(self):
        """
        Calcule la somme des _statistique du personnage
        :return: Les statistiques finale du personne, donc somme de ses
        sattistques de base et celles de son personnage et son armure
        :rtype: Statistique
        """
        if self.arme:
            statistique_totale = self.statistique + self.arme.statistique_bonus
        if self.armure:
            statistique_totale = self.statistique + self.armure.statistique_bonus
        return statistique_totale

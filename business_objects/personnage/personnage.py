MAGICIEN = "magicien"
GUERRIER = "guerrier"
VOLEUR = "voleur"


class Personnage:
    def __init__(self, personnage_type):
        self.__personnage_type = personnage_type

    @property
    def personnage_type(self):
        """
        Grâce au décorateur @property, je défini une propriété
        personnage_type pour ma classe. Elle va émuler une attribut public
        personnage_type mais va déclencher en fait cette fonction qui va agir
        comme un setter C'est plus "pythonic" que de faire des getter/setter
        comme en java par exemple.

        :return: le type du personnage
        :rtype: str
        """
        return self.__personnage_type

    @personnage_type.setter
    def personnage_type(self,type):
        """
        Grâce au décorateur @property, je défini une propriété
        personnage_type pour ma classe. Elle va émuler une attribut public
        personnage_type mais va déclencher en fait cette fonction qui va agir
        comme un setter C'est plus "pythonic" que de faire des getter/setter
        comme en java par exemple.

        :param type: le nouveau type du personnage
        :type type: str
        :return: None
        """
        self.__personnage_type = type
        return None
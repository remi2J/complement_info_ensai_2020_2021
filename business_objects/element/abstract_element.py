from abc import ABC

from business_objects.statistique import Statistique


class AbstractElement(ABC):
    """
    Classe abstraite représentant un élément de notre jeu. Cela peut être
    aussi bien un personnage qu'un monstre.
    Cela va nous permettre de centraliser tout le code similaire aux deux
    type d'objet
    """

    def __init__(self
                 , nom=""
                 , force=0
                 , agilite=0
                 , magie=0
                 , defense=0
                 , points_de_vie=0):
        """
            Constructeur commun à tous les éléments du jeu
            :param nom : le _nom de de l'élément
            :type non : str
            :param force: la force de l'élément
            :type force: int
            :param agilite: l'agilite de l'élément
            :type agilite: int
            :param magie: la magie de l'élément
            :type magie: int
            :param defense: la défense de l'élément
            :type defense: int
            :param points_de_vie: les point de vie de l'élément
            :type points_de_vie: int
            """
        self._nom = nom
        self._statistique = Statistique(force, agilite, magie, defense,
                                        points_de_vie)


    @property
    def force(self):
        """Permet d'accéder à l'attribut force de _statistique facilement"""
        return self._statistique.force

    @force.setter
    def force(self, nouvelle_force):
        """Permet de modifier l'attribut force de _statistique facilement"""
        self._statistique.force = nouvelle_force

    @property
    def agilite(self):
        """Permet d'accéder à l'attribut agilite de _statistique facilement"""
        return self._statistique.agilite

    @agilite.setter
    def agilite(self, nouvelle_agilite):
        """Permet de modifier l'attribut agilite de _statistique facilement"""
        self._statistique.agilite = nouvelle_agilite

    @property
    def magie(self):
        """Permet d'accéder à l'attribut magie de _statistique facilement"""
        return self._statistique.magie

    @magie.setter
    def magie(self, nouvelle_magie):
        """Permet de modifier l'attribut magie de _statistique facilement"""
        self._statistique.magie = nouvelle_magie

    @property
    def defense(self):
        """Permet d'accéder à l'attribut defense de _statistique facilement"""
        return self._statistique.defense

    @defense.setter
    def defense(self, nouvelle_defense):
        """Permet de modifier l'attribut defense de _statistique facilement"""
        self._statistique.defense = nouvelle_defense

    @property
    def points_de_vie(self):
        """Permet d'accéder à l'attribut points_de_vie de _statistique facilement"""
        return self._statistique.defense

    @points_de_vie.setter
    def points_de_vie(self, nouveaux_points_de_vie):
        """Permet de modifier l'attribut points_de_vie de _statistique facilement"""
        self._statistique.points_de_vie = nouveaux_points_de_vie
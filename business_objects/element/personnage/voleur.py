from business_objects.objet.arme.arc import Arc
from business_objects.objet.arme.couteau import Couteau
from business_objects.element.personnage.abstract_personnage import \
    AbstractPersonnage


class Voleur(AbstractPersonnage):

    armes_atorisees = [Couteau, Arc]

    def __init__(self
                 , nom=""
                 , arme=None
                 , armure=None
                 , force=0
                 , agilite=0
                 , magie=0
                 , defense=0
                 , points_de_vie=0):
        super().__init__(nom=nom
                         , arme=arme
                         , armure=armure
                         , force=force
                         , agilite=agilite
                         , magie=magie
                         , defense=defense
                         , points_de_vie=points_de_vie)

    def attaque(self):
        # code spécifique au voleur ici
        return self.arme.utiliser_arme(self._statistique)

    def defense(self, attaque_info):
        # code spécifique au voleur ici
        return self.armure.utiliser_armure(self._statistique, attaque_info)

    def get_armes_autorisees(self):
        return Voleur.armes_atorisees
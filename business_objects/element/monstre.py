from business_objects.element.abstract_element import AbstractElement


class Monstre(AbstractElement):
    """
    Classe qui va contenir un monstre de notre jeu
    """

    def __init__(self
                 , nom=""
                 , description=""
                 , type_monstre=""
                 , points_de_vie=0
                 , force=0
                 , magie=0
                 , agilite=0
                 , defense=0):
        super().__init__(nom=nom
                         , force=force
                         , agilite=agilite
                         , magie=magie
                         , defense=defense
                         , points_de_vie=points_de_vie)
        self.__description = description
        self.__type_monstre = type_monstre

    @property
    def description(self):
        return self.__description

    @description.setter
    def description(self, new_description):
        self.__description = new_description


    @property
    def type_monstre(self):
        return self.__type_monstre

    @type_monstre.setter
    def type_monstre(self, new_type_monstre):
        self.__type_monstre = new_type_monstre
from business_objects.element.abstract_element import AbstractElement
from business_objects.monstre.type_monstre import TypeMonstre
from configuration.properties import url_monster


class Monstre(AbstractElement):
    """
    Classe qui va contenir un monstre de notre jeu
    """

    def __init__(self
                 , id=None
                 , nom=None
                 , description=None
                 , id_type_monstre=None
                 , label_type_monstre=None
                 , points_de_vie=None
                 , force=None
                 , magie=None
                 , agilite=None
                 , defense=None):
        super().__init__(nom=nom
                         , force=force
                         , agilite=agilite
                         , magie=magie
                         , defense=defense
                         , points_de_vie=points_de_vie)
        self.__id = id
        self.__description = description
        self.__type_monstre = TypeMonstre(id_type=id_type_monstre,
                                          label=label_type_monstre)

    @property
    def url(self):
        return url_monster + "/" + self.nom

    @property
    def all_attributes(self):
        return dict(vars(self), url=self.url)

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, new_id):
        self.__id = new_id

    @property
    def type_monstre(self):
        """

        :return:
        :rtype: TypeMonstre
        """
        return self.__type_monstre

    @type_monstre.setter
    def type_monstre(self, new_type_monstre):
        self.__type_monstre = new_type_monstre

    @property
    def description(self):
        return self.__description

    @description.setter
    def description(self, new_description):
        self.__description = new_description

    @property
    def nom(self):
        return self._nom

    @nom.setter
    def nom(self, new_nom):
        self._nom = new_nom

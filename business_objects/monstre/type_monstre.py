class TypeMonstre:
    """
    Simple couple id / __label qui représente les types de montre en base
    """

    def __init__(self, label, id_type=None):
        self.__id_type = id_type
        self.__label = label


    @property
    def id_label(self):
        return self.__id_type

    @id_label.setter
    def id_label(self, new_id_label):
        self.__id_type = new_id_label

    @property
    def label(self):
        return self.__label

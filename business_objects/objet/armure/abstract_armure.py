from abc import abstractmethod

from business_objects.attaque_info import AttaqueInfo
from business_objects.objet.abstract_equipement import AbstractEquipement
from business_objects.statistique import Statistique


class AbstractArmure(AbstractEquipement):
    """
    Classe abstraite représentant le concept d'"personnage". ELle devrait être
    héritée par les classe qui veulent être considérée comme des armes.
    """
    def __init__(self, nom, id,force_bonus, agilite_bonus, magie_bonus,
                 defense_bonus, points_de_vie_bonus, description_defense,
                 base_defense):
        super().__init__(nom=nom,
                         id=id,
                         force_bonus=force_bonus,
                         agilite_bonus=agilite_bonus,
                         magie_bonus=magie_bonus,
                         defense_bonus=defense_bonus,
                         points_de_vie_bonus=points_de_vie_bonus)
        self._description_defense = description_defense
        self._base_defense = base_defense

    @abstractmethod
    def utiliser_armure(self, statistique_pers, degat_attaque):
        """
        Détermine la défenser d'une armure. Chaque armure doit surcharger cette
        méthode pour donner son comportement spécifique
        :param statistique_pers: les stats du personnage qui désire utiliser
        l'armure
        :type statistique_pers: Statistique
        :param degat_attaque: les dégâts que devrait subir le personnage
        :type degat_attaque: int
        :return: le nombre de dégât réduit
        :rtype: int
        """

    @property
    def description_defense(self):
        return self._description_defense

    @property
    def base_defense(self):
        return self._base_defense

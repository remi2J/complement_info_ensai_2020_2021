from business_objects.objet.armure.abstract_armure import AbstractArmure


class ArmureLegere(AbstractArmure):
    def __init__(self, nom, force_bonus, agilite_bonus, magie_bonus,
                 defense_bonus, points_de_vie_bonus, description_defense,
                 base_defense, id=None):
        super().__init__(nom=nom,
                         id=id,
                         force_bonus=force_bonus,
                         agilite_bonus=agilite_bonus,
                         magie_bonus=magie_bonus,
                         defense_bonus=defense_bonus,
                         points_de_vie_bonus=points_de_vie_bonus,
                         description_defense=description_defense,
                         base_defense=base_defense)

    def utiliser_armure(self, statistique_pers, degat_attaque):
        degats_finaux = degat_attaque - (
                statistique_pers.agilite + statistique_pers.defense) * \
                       self.base_defense / 100
        return degats_finaux

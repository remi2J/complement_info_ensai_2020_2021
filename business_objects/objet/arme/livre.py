from business_objects.attaque_info import AttaqueInfo
from business_objects.objet.arme.abstract_arme import AbstractArme


class Livre(AbstractArme):

    DATABASE_TYPE_LABEL = "livre"

    def __init__(self, nom, force_bonus, agilite_bonus, magie_bonus,
                 defense_bonus, points_de_vie_bonus, description_attaque,
                 base_attaque, id=None):
        super().__init__(nom=nom,
                         id=id,
                         force_bonus=force_bonus,
                         agilite_bonus=agilite_bonus,
                         magie_bonus=magie_bonus,
                         defense_bonus=defense_bonus,
                         points_de_vie_bonus=points_de_vie_bonus,
                         description_attaque=description_attaque,
                         base_attaque=base_attaque,
                         data_base_type_label=Livre.DATABASE_TYPE_LABEL)

    def utiliser_arme(self,stat_pers):
        degat_inflige = (stat_pers.magie * 2 * self.base_attaque)
        return degat_inflige
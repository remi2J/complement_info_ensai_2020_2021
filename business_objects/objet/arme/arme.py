from abc import abstractmethod

from business_objects.attaque_info import AttaqueInfo
from business_objects.objet.abstract_equipement import AbstractEquipement


class Arme(AbstractEquipement):
    """
    Classe abstraite représentant le concept d'"arme". ELle devrait être
    héritée par les classe qui veulent être considérée comme des armes.
    """

    def __init__(self, nom, force_bonus, agilite_bonus, magie_bonus,
                 defense_bonus, point_de_vie_bonus,
                 base_attaque):
        super().__init__(nom=nom,
                         force_bonus=force_bonus,
                         agilite_bonus=agilite_bonus,
                         magie_bonus=magie_bonus,
                         defense_bonus=defense_bonus,
                         point_de_vie_bonus=point_de_vie_bonus)
        self._base_attaque = base_attaque


    @property
    def base_attaque(self):
        return self._base_attaque
from abc import abstractmethod

from business_objects.objet.abstract_equipement import AbstractEquipement
from configuration.properties import url_arme


class AbstractArme(AbstractEquipement):
    """
    Classe abstraite représentant le concept d'"personnage". ELle devrait être
    héritée par les classe qui veulent être considérée comme des armes.
    """

    def __init__(self, nom, id, force_bonus, agilite_bonus, magie_bonus,
                 defense_bonus, points_de_vie_bonus, description_attaque,
                 base_attaque, data_base_type_label):
        super().__init__(nom=nom,
                         id=id,
                         force_bonus=force_bonus,
                         agilite_bonus=agilite_bonus,
                         magie_bonus=magie_bonus,
                         defense_bonus=defense_bonus,
                         points_de_vie_bonus=points_de_vie_bonus)
        self._description_attaque = description_attaque
        self._base_attaque = base_attaque
        self.DATABASE_TYPE_LABEL = data_base_type_label

    @abstractmethod
    def utiliser_arme(self, statistique_pers):
        """
        Détermine comment une personnage s'utilise. Chaque personnage doit surcharger cette
        méthode pour donner son comportement spécifique
        :param statistique_pers: les stats du personnage qui désire utiliser
        l'personnage
        :type statistique_pers: Statistique
        :return: le nombre de dégpat infligé
        :rtype: int
        """

    @property
    def description_attaque(self):
        return self._description_attaque

    @description_attaque.setter
    def description_attaque(self, new_description_attaque):
        self._description_attaque = new_description_attaque

    @property
    def base_attaque(self):
        return self._base_attaque

    @base_attaque.setter
    def base_attaque(self, new_base_attaque):
        self._base_attaque = new_base_attaque

    @property
    def url(self):
        return url_arme +"/"+ self.nom



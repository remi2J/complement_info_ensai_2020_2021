from abc import ABC

from business_objects.statistique import Statistique


class AbstractEquipement(ABC):
    """
    Doit être héritée par toute classe qui représente un équipement.
    """
    def __init__(self, nom, id, force_bonus, agilite_bonus, magie_bonus,
                 defense_bonus, points_de_vie_bonus):
        self.__nom = nom
        self.__id = id
        self.__statistique_bonus = Statistique(
            force=force_bonus,
            agilite=agilite_bonus,
            magie=magie_bonus,
            defense=defense_bonus,
            points_de_vie=points_de_vie_bonus)

    @property
    def nom(self):
        return self.__nom

    @nom.setter
    def nom(self, new_nom):
        self.__nom = new_nom

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, new_id):
        self.__id = new_id

    @property
    def force(self):
        """Permet d'accéder à l'attribut force de _statistique facilement"""
        return self.__statistique_bonus.force

    @force.setter
    def force(self, nouvelle_force):
        """Permet de modifier l'attribut force de _statistique facilement"""
        self.__statistique_bonus.force = nouvelle_force

    @property
    def agilite(self):
        """Permet d'accéder à l'attribut agilite de _statistique facilement"""
        return self.__statistique_bonus.agilite

    @agilite.setter
    def agilite(self, nouvelle_agilite):
        """Permet de modifier l'attribut agilite de _statistique facilement"""
        self.__statistique_bonus.agilite = nouvelle_agilite

    @property
    def magie(self):
        """Permet d'accéder à l'attribut magie de _statistique facilement"""
        return self.__statistique_bonus.magie

    @magie.setter
    def magie(self, nouvelle_magie):
        """Permet de modifier l'attribut magie de _statistique facilement"""
        self.__statistique_bonus.magie = nouvelle_magie

    @property
    def defense(self):
        """Permet d'accéder à l'attribut defense de _statistique facilement"""
        return self.__statistique_bonus.defense

    @defense.setter
    def defense(self, nouvelle_defense):
        """Permet de modifier l'attribut defense de _statistique facilement"""
        self.__statistique_bonus.defense = nouvelle_defense

    @property
    def points_de_vie(self):
        """Permet d'accéder à l'attribut points_de_vie de _statistique facilement"""
        return self.__statistique_bonus.defense

    @points_de_vie.setter
    def points_de_vie(self, nouveaux_points_de_vie):
        """Permet de modifier l'attribut points_de_vie de _statistique facilement"""
        self.__statistique_bonus.points_de_vie = nouveaux_points_de_vie
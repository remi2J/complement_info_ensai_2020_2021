class Statistique:
    """
    Va contenir les statistiques des personnages, des armes ou même des
    monstres. Ainsi on s'assure de la cohérence de la gestion des statistiques.
    """
    def __init__(self, force, agilite, magie, defense, points_de_vie):
        self.__force = force
        self.__agilite = agilite
        self.__magie = magie
        self.__defense = defense
        self.__points_de_vie = points_de_vie

    def __add__(self, other):
        """
        :param other: une _statistique avec qui on va faire une addition
        :type other: Statistique
        :return: la somme des deux statistiques terme à terme
        :rtype: Statistique
        """
        if isinstance(other, Statistique):
            return Statistique(
                force=self.force + other.force
                , agilite=self.agilite + other.agilite
                , magie=self.magie + other.magie
                , defense=self.defense + other.defense
                , points_de_vie=self.points_de_vie + other.points_de_vie
            )
        else:
            self


    def __radd__(self, other):
        """
        :param other: une _statistique avec qui on va faire une addition
        :type other: Statistique
        :return: la somme des deux statistiques terme à terme
        :rtype: Statistique
        """
        if not isinstance(other, Statistique):
            return self
        else:
            return self.__add__(other)


    @property
    def force(self):
        return self.__force

    @force.setter
    def force(self, force):
        self.__force = force

    @property
    def agilite(self):
        return self.__agilite

    @agilite.setter
    def agilite(self, agilite):
        self.__agilite = agilite

    @property
    def magie(self):
        return self.__magie

    @magie.setter
    def magie(self, magie):
        self.__magie = magie

    @property
    def defense(self):
        return self.__defense

    @defense.setter
    def defense(self, defense):
        self.__defense = defense

    @property
    def points_de_vie(self):
        return self.__points_de_vie

    @points_de_vie.setter
    def points_de_vie(self, points_de_vie):
        self.__points_de_vie = points_de_vie

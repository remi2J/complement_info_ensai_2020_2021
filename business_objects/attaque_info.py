class AttaqueInfo:
    """
    Contient les informations nécessaires pour décrire une attaque. On
    considère comme "attaque" l'ensemble attaque + défense.

    """
    def __init__(self, phrase_attaque, degats_attaque):
        """
        :param phrase_attaque: le phrase associée à l'attaque
        :type phrase_attaque: str
        :param degat_attaque: les dégâts pur de l'attaque
        :type degat_attaque: int
        """
        self.__phrase_attaque = phrase_attaque
        self.__degats_attaque = degats_attaque
        self.__phrase_defense = None
        self.__degats_finaux = None

    @property
    def phrase_attaque(self):
        """
        Grâce au décorateur @property, je défini une propriété
        phrase_attaque pour ma classe. Elle va émuler une attribut public
        personnage_type mais va déclencher en fait cette fonction qui va agir
        comme un setter C'est plus "pythonic" que de faire des getter/setter
        comme en java par exemple.

        :return: la phrase de l'attaque
        :rtype: str
        """
        return self.__phrase_attaque

    @property
    def degats_attaque(self):
        """
        Grâce au décorateur @property, je défini une propriété
        phrase_attaque pour ma classe. Elle va émuler une attribut public
        personnage_type mais va déclencher en fait cette fonction qui va agir
        comme un setter C'est plus "pythonic" que de faire des getter/setter
        comme en java par exemple.

        :return: les dégâts de l'attaque
        :rtype: int
        """
        return self.__degats_attaque

    @property
    def phrase_defense(self):
        """
        :return: la phrase de l'attaque
        :rtype: str
        """
        return self.__phrase_defense

    @phrase_defense.setter
    def phrase_defense(self, phrase_defense):
        """
        Met à jour la phrase de la défense
        :return: None
        """
        self.__phrase_defense = phrase_defense

    @property
    def degats_finaux(self):
        """
        :return: les dégâts finaux de l'attaque
        :rtype: int
        """
        return self.__degats_finaux

    @degats_finaux.setter
    def degats_finaux(self, degats_finaux):
        """
        Met à jour les dégâts finaux de l'attaque
        :return: None
        """
        self.__degats_finaux = degats_finaux

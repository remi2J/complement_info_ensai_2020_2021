DROP SEQUENCE IF EXISTS  id_objet CASCADE;
CREATE SEQUENCE id_objet;

DROP TABLE IF EXISTS type_arme CASCADE;
CREATE TABLE type_arme
(
    type_arme_id serial,
    type_arme_label text COLLATE pg_catalog."default" UNIQUE,
    CONSTRAINT type_arme_pkey PRIMARY KEY (type_arme_id)
);

INSERT INTO type_arme (type_arme_id, type_arme_label) VALUES
(1,'arc'),
(2,'baton'),
(3,'couteau'),
(4,'epee'),
(5,'lance'),
(6,'livre');

DROP TABLE IF EXISTS statistique_generale CASCADE;
CREATE TABLE statistique_generale (
    id_statistique serial ,
    force integer,
    agilite integer,
    magie integer,
    defense integer,
    points_de_vie integer,
    CONSTRAINT arme_statistique_generale PRIMARY KEY (id_statistique)
);

DROP TABLE IF EXISTS arme CASCADE;
CREATE TABLE arme
(
    id_arme integer NOT NULL DEFAULT nextval('id_objet'),
    id_label integer,
    nom text COLLATE pg_catalog."default",
    description_attaque text COLLATE pg_catalog."default",
    base_attaque integer,
    CONSTRAINT arme_pkey PRIMARY KEY (id_arme),
    CONSTRAINT arme_label_fkey FOREIGN KEY (id_label) REFERENCES type_arme(type_arme_id)
)INHERITS (statistique_generale);

DROP TYPE IF EXISTS type_perso;
CREATE TYPE type_perso AS ENUM ('guerrier', 'voleur', 'magicien');

DROP TABLE IF EXISTS personnage CASCADE;
CREATE TABLE personnage (
    id_personnage serial,
    id_arme integer,
    type_personnage type_perso,
    CONSTRAINT arme_statistique_personnage PRIMARY KEY (id_personnage)
)INHERITS (statistique_generale);




--Quelques données

INSERT INTO arme (id_arme, id_label, nom, description_attaque, base_attaque,
force, agilite, magie, defense, points_de_vie) VALUES
(1,4, 'master sword', 'Un eclair de lumière pourfend l''adversaire', 100,35,10,10,15,50) )
, (2, 5, 'trident de Poseidon', 'Un raz-de-marée noie l''adversaire', 150,60,0,42,12,50);


INSERT INTO personnage (id_personnage, type_personnage,id_arme, force, agilite, magie, defense, points_de_vie)) VALUES
(2, 'guerrier',1, 120,100,10,150,500),
(3, 'guerrier',2,30,150,98,35,650);

